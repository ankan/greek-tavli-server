void roll(chan * c)
{
    bzero(c->dice, 5);
    c->dice[0] = 1 + (char) (6.0 * rand() / (RAND_MAX - 1.0));
    c->dice[1] = 1 + (char) (6.0 * rand() / (RAND_MAX - 1.0));
    if (c->dice[0] == c->dice[1]) {
	c->dice[2] = c->dice[3] = c->dice[0];
    }
}

/*--It is domove() & checkmove in once --*/
/*--returns pos if move is valid, -1 if invalid --*/
int domove(col * board, short start, short dice, short color, short game)
{
    short bhome, whome, blim, wlim, i, dest;
    if (board[start].top != color) {
	return (-1);
    }
    switch (game) {
    case PORTES:{
	    bhome = 25;
	    whome = 0;
	    blim = 19;
	    wlim = 6;
	    switch (color) {
	    case BLACK:{
		    int bmin = 0;
		    dest = start + dice;

		    /*-- If poulia on takos can't play --*/
		    if (board[whome].bpop && start != whome) {
			return (-1);
		    }

		    for (i = 24; i > 0; i--) {
			if (board[i].top == BLACK) {
			    bmin = i;
			}
		    }
		    if (start == bmin && dest > bhome) {
			dest = bhome;
		    }

		    /*-- mazema. Valid or not?-----------*/
		    if (dest == bhome && bmin < blim) {
			return (-1);
		    }
		    if (dest > bhome) {
			return (-1);
		    }

		    /*-- Valid mazema. No more checks --*/
		    if (dest == bhome) {
			if (--board[start].bpop == 0) {
			    board[start].top = 0;
			}
			board[dest].bpop++;

			return (dest);
		    }

		    if (board[dest].wpop < 2) {
			if (board[dest].top == WHITE) {
			    board[bhome].wpop++;
			    board[dest].wpop--;
			    board[bhome].top = WHITE;
			}
			if (--board[start].bpop == 0) {
			    board[start].top = 0;
			}
			board[dest].bpop++;
			if (dest != bhome) {

			/*-- never set top at home. May be there
			 *-- is opponent takos there and it nust be
                         *-- computed by cmtoex() --*/
			    board[dest].top = BLACK;
			}
			return (dest);
		    }
		    return (-1);
		}
	    case WHITE:{
		    int wmax = 0;
		    dice *= -1;
				/*-- White moves backwards in portes --*/
		    dest = start + dice;
		    if (board[bhome].wpop && start != bhome) {
			return (-1);
		    }
		    for (i = 1; i < 25; i++) {
			if (board[i].top == WHITE) {
			    wmax = i;
			}
		    }
		    if (start == wmax && dest < whome) {
			dest = whome;
		    }
		    if (dest == whome && wmax > wlim) {
			return (-1);
		    }
		    if (dest < whome) {
			return (-1);
		    }

		    /*-- Valid mazema. No more checks --*/
		    if (dest == whome) {
			if (--board[start].wpop == 0) {
			    board[start].top = 0;
			}
			board[dest].wpop++;

			return (dest);
		    }
		    if (board[dest].bpop < 2) {
			if (board[dest].top == BLACK) {
			    board[whome].bpop++;
			    board[dest].bpop--;
			    board[whome].top = BLACK;
			}
			if (--board[start].wpop == 0) {
			    board[start].top = 0;
			}
			board[dest].wpop++;
			if (dest != whome) {
			    board[dest].top = WHITE;
			}
			return (dest);
		    }
		}
	    }
	    return (-1);
	}
    case PLAKWTO:{
	    bhome = 0;
	    whome = 25;
	    blim = 6;
	    wlim = 19;
	    switch (color) {
	    case BLACK:{
		    int bmax = 0;
		    int blocked = 0;
		    dice *= -1;
				/*-- black moves backwards in plakwto --*/
		    dest = start + dice;
		    for (i = 1; i < 25; i++) {
			if (board[i].top == BLACK) {
			    bmax = i;
			} else {
			    if (board[i].bpop) {
				blocked++;
			    }
			}
		    }
		    if (start == bmax && dest < bhome) {
			dest = bhome;
		    }
		    if (dest == bhome && (blocked || bmax > blim)) {
			return (-1);
		    }
		    if (dest < bhome) {
			return (-1);
		    }
		    if (board[dest].top == WHITE && (board[dest].wpop > 1 || board[dest].bpop > 0)) {
			return (-1);
		    }
		    if (--board[start].bpop == 0) {
			if (board[start].wpop == 0) {
			    board[start].top = 0;
			} else {
			    board[start].top = WHITE;
			}
		    }
		    board[dest].bpop++;
		    board[dest].top = BLACK;
		    return (dest);
		}
	    case WHITE:{
		    int wmin = 0;
		    int blocked = 0;
		    dest = start + dice;
		    for (i = 24; i > 0; i--) {
			if (board[i].top == WHITE) {
			    wmin = i;
			} else {
			    if (board[i].wpop) {
				blocked++;
			    }
			}
		    }
		    if (start == wmin && dest > whome) {
			dest = whome;
		    }
		    if (dest == whome && (blocked || wmin < wlim)) {
			return (-1);
		    }
		    if (dest > whome) {
			return (-1);
		    }
		    if (board[dest].top == BLACK && (board[dest].bpop > 1 || board[dest].wpop > 0)) {
			return (-1);
		    }
		    if (--board[start].wpop == 0) {
			if (board[start].bpop == 0) {
			    board[start].top = 0;
			} else {
			    board[start].top = BLACK;
			}
		    }
		    board[dest].wpop++;
		    board[dest].top = WHITE;
		    return (dest);
		}
	    }
	    return (-1);
	}
    case FEYGA:{
	    bhome = 0;
	    whome = 12;
		       /*-- only for this function --*/
	    blim = 6;
	    dice *= -1;/*-- Everybody moves backwards --*/
	    switch (color) {
	    case BLACK:{
		    int bmax = 0;
		    int bmin = 0;
		    dest = start + dice;
		    for (i = 1; i <= 24; i++) {
			if (board[i].top == BLACK) {
			    bmax = i;
			}
		    }
		    for (i = 24; i > 0; i--) {
			if (board[i].top == BLACK) {
			    bmin = i;
			}
		    }
		    if (start == 24 && board[24].bpop == 14 && bmin > 12) {
			return (-1);
		    }
		    if (start == bmax && dest < bhome) {
			dest = bhome;
		    }
		    if (dest == bhome && bmax > blim) {
			return (-1);
		    }
		    if (dest < bhome) {
			return (-1);
		    }
		    if (board[dest].top == WHITE) {
			return (-1);
		    }
		    if (--board[start].bpop == 0) {
			board[start].top = 0;
		    }
		    board[dest].bpop++;
		    board[dest].top = BLACK;
		    return (dest);
		}
	    case WHITE:{
		    short wlimup = 18;
		    short wlimd = 13;
		    short wmin = 0;
		    short wmax = 0;
		    dest = start + dice;
		    dest = dest < 1 ? (dest + 24) : dest;
		    for (i = 24; i > 0; i--) {
			if (board[i].top == WHITE) {
			    wmin = i;
			}
		    }
		    for (i = 1; i <= 24; i++) {
			if (board[i].top == WHITE) {
			    wmax = i;
			}
		    }
		    if (start == 12 && board[12].wpop == 14 && wmax < 13) {
			return (-1);
		    }
		    if (wmin >= wlimd && wmax <= wlimup && dest <= whome) {
			if (start == wmax) {
			    dest = whome;
			}
			if (dest < whome) {
			    return (-1);
			}
		    } else if (start > whome && dest <= whome) {
			return (-1);
		    }
		    if (dest == whome && (wmin < wlimd || wmax > wlimup)) {
			return (-1);
		    }

		    dest = dest == whome ? 25 : dest;
		     /*-- Here was a fucking bug:
			  Testing if board[dest].top == BLACK
			  before setting dest to 25 if dest == whome.
			  What a shame...
		    */
		    if (board[dest].top == BLACK) {
			return (-1);
		    }
		    if (--board[start].wpop == 0) {
			board[start].top = 0;
		    }
		    board[dest].wpop++;
		    board[dest].top = WHITE;
		    return (dest);
		}
	    }
	    return (-1);
	}
    }
    return (-1);
}

/*-- returns how many moves we expect from player */
int cmtoex(chan * c, short recursion)
{
    short whome, bhome, bmax, wmax, bmin, wmin, color, mazema;
    short takos = 0, takospos;
    col test[26];
    short i, j, zaries = 0, takozaries = 0, s, k, p;
    char dice[5];
    short dcount[4];
    short dest;
    memcpy(test, c->board, sizeof(test));
    color = c->turn == c->blackpos ? BLACK : WHITE;
    strcpy(dice, c->dice);
    bzero(dcount, sizeof(dcount));
    switch (c->game) {
    case PORTES:{
	    col test2[26];
	    short dcount2[4];
	    bhome = 25;
	    whome = 0;
	    if (color == BLACK) {
		takos = c->board[whome].bpop;
		takospos = whome;
	    } else {
		takos = c->board[bhome].wpop;
		takospos = bhome;
	    }
	    i = 0;
	    while (takos && dice[i]) {
		if (domove(test, takospos, dice[i], color, PORTES) > -1) {
		    takozaries++;
		    takos--;
		    dcount[i]++;
		}
		i++;
	    }
	    if (takos) {
		return (takozaries);
	    }
	    memcpy(test2, test, sizeof(test2));
	    memcpy(dcount2, dcount, sizeof(dcount2));
	    k = i;
	    mazema = 0;
	    for (j = 1; j < 25; j++) {
		s = c->turn == c->blackpos ? test[j].bpop : test[j].wpop;
		if (!s)
		    continue;

		if (!mazema || s > 1) {
		    i = k;
		} else {
		    i = dcount[0] > dcount[1] ? 1 : 0;
		}

		while (s && dice[i]) {
		    if ((dest = domove(test, j, dice[i], color, c->game)) > -1) {
			int r = 1;
			s--;
			while (dice[i + r]) {
			    if (dest != 0 && dest != 25 && (dest = domove(test, dest, dice[i + r], color, c->game)) > -1) {
				dcount[i + r]++;
			    }
			    r++;
			}
			dcount[i]++;
		    }
		    if (dest != 0 && dest != 25 && dice[0] != dice[2]) {
			if (color == WHITE) {
			    for (p = 1, wmax = 0; p < 25; p++) {
				if (test[p].wpop) {
				    wmax = p;
				}
			    }
			    if (wmax > 6) {
				memcpy(test, test2, sizeof(test));
			    } else {
				mazema = 1;
			    }
			} else {
			       /*-- BLACK --*/
			    for (p = 24, bmin = 24; p > 0; p--) {
				if (test[p].bpop) {
				    bmin = p;
				}
			    }
			    if (bmin < 19) {
				memcpy(test, test2, sizeof(test));
			    } else {
				mazema = 1;
			    }
			}
		    }
		    i++;
		}
	    }
	    if (strlen(dice) > 2) {
		zaries = (dcount[0] + dcount[1] + dcount[2] + dcount[3]);
		dcount[0] = dcount[1] = dcount[2] = dcount[3] = 0;
	    } else {
		for (i = 0; i < 4; i++) {
		    if (dcount[i]) {
			dcount[i] = 0;
			zaries++;
		    }
		}
	    }
	    zaries = zaries > strlen(dice) ? strlen(dice) : zaries;

/*--- Rorate tavliera otherwise. Sometimes usefull on mazema --*/
	    if (zaries < strlen(dice)) {
		int zaries2 = 0;
		memcpy(dcount, dcount2, sizeof(dcount));
		memcpy(test, test2, sizeof(test));
		mazema = 0;
		for (j = 24; j > 0; j--) {
		    s = c->turn == c->blackpos ? test[j].bpop : test[j].wpop;
		    if (!s)
			continue;
		    if (!mazema || s > 1) {
			i = k;
		    } else {
			i = dcount[0] > dcount[1] ? 1 : 0;
		    }

		    while (s && dice[i]) {
			if ((dest = domove(test, j, dice[i], color, c->game)) > -1) {
			    int r = 1;
			    s--;
			    while (dice[i + r]) {
				if (dest != 0 && dest != 25 && (dest = domove(test, dest, dice[i + r], color, c->game)) > -1) {
				    dcount[i + r]++;
				}
				r++;
			    }
			    dcount[i]++;
			}
			if (dest != 0 && dest != 25 && dice[0] != dice[2]) {
			    if (color == WHITE) {
				for (p = 1, wmax = 0; p < 25; p++) {
				    if (test[p].wpop) {
					wmax = p;
				    }
				}
				if (wmax > 6) {
				    memcpy(test, test2, sizeof(test));
				} else {
				    mazema = 1;
				}
			    } else {
			       /*-- BLACK --*/
				for (p = 24, bmin = 24; p > 0; p--) {
				    if (test[p].bpop) {
					bmin = p;
				    }
				}
				if (bmin < 19) {
				    memcpy(test, test2, sizeof(test));
				} else {
				    mazema = 1;
				}
			    }

			}
			i++;
		    }
		}
		if (strlen(dice) > 2) {
		    zaries2 = (dcount[0] + dcount[1] + dcount[2] + dcount[3]);
		    dcount[0] = dcount[1] = dcount[2] = dcount[3] = 0;
		} else {
		    for (i = 0; i < 4; i++) {
			if (dcount[i]) {
			    dcount[i] = 0;
			    zaries2++;
			}
		    }
		}
		zaries2 = zaries2 > strlen(dice) ? strlen(dice) : zaries2;
		zaries = zaries2 > zaries ? zaries2 : zaries;
	    }
	    if (zaries < strlen(dice) && strlen(dice) == 2) {

		/*--swap dice and try again --*/
		char d;
		int zaries2 = 0;
		d = c->dice[0];
		c->dice[0] = c->dice[1];
		c->dice[1] = d;

		/*-- recursive call with swaped dice --*/
		if (recursion) {
		    zaries2 = cmtoex(c, 0);
		    return zaries > zaries2 ? zaries : zaries2;
		}
	    }
	    return zaries;
	}
    case PLAKWTO:
	i = 0;
	mazema = 0;
	for (j = 1; j < 25; j++) {
	    s = c->turn == c->blackpos ? test[j].bpop : test[j].wpop;
	    if (!s)
		continue;
	    if (!mazema || s > 1) {
		i = 0;
	    } else {
		i = dcount[0] > dcount[1] ? 1 : 0;
	    }

	    while (s && dice[i]) {
		if ((dest = domove(test, j, dice[i], color, c->game)) > -1) {
		    int r = 1;
		    s--;
		    while (dice[i + r]) {
			if (dest != 0 && dest != 25 && (dest = domove(test, dest, dice[i + r], color, c->game)) > -1) {
			    dcount[i + r]++;
			}
			r++;
		    }
		    dcount[i]++;
		}
		if ((dest != 0 && dest != 25 && dice[0] != dice[2])) {
		    if (color == BLACK) {
			for (p = 1, bmax = 0; p < 25; p++) {
			    if (test[p].bpop) {
				bmax = p;
			    }
			}
			if (bmax > 6) {
			    memcpy(test, c->board, sizeof(test));
			} else {
			    mazema = 1;
			}
		    } else {   /*-- WHITE --*/
			for (p = 24, wmin = 24; p > 0; p--) {
			    if (test[p].wpop) {
				wmin = p;
			    }
			}
			if (wmin < 19) {
			    memcpy(test, c->board, sizeof(test));
			} else {
			    mazema = 1;
			}
		    }

		}
		i++;
	    }
	}
	if (strlen(dice) == 4) {
	    zaries = dcount[0] + dcount[1] + dcount[2] + dcount[3];
	    dcount[0] = dcount[1] = dcount[2] = dcount[3] = 0;
	} else {
	    for (i = 0; i < 4; i++) {
		if (dcount[i]) {
		    dcount[i] = 0;
		    zaries++;
		}
	    }
	}
	zaries = zaries > strlen(dice) ? strlen(dice) : zaries;

/*--- Rorate tavliera otherwise. Sometimes usefull on mazema --*/
	if (zaries < strlen(dice)) {
	    int zaries2 = 0;
	    memcpy(test, c->board, sizeof(test));
	    bzero(dcount, sizeof(dcount));
	    mazema = 0;
	    for (j = 24; j > 0; j--) {
		s = c->turn == c->blackpos ? test[j].bpop : test[j].wpop;
		if (!s)
		    continue;
		if (!mazema || s > 1) {
		    i = 0;
		} else {
		    i = dcount[0] > dcount[1] ? 1 : 0;
		}
		while (s && dice[i]) {
		    if ((dest = domove(test, j, dice[i], color, c->game)) > -1) {
			int r = 1;
			s--;
			while (dice[i + r]) {
			    if (dest != 0 && dest != 25 && (dest = domove(test, dest, dice[i + r], color, c->game)) > -1) {
				dcount[i + r]++;
			    }
			    r++;
			}
			dcount[i]++;
		    }
		    if (dest != 0 && dest != 25 && dice[0] != dice[2]) {
			if (color == BLACK) {
			    for (p = 1, bmax = 0; p < 25; p++) {
				if (test[p].bpop) {
				    bmax = p;
				}
			    }
			    if (bmax > 6) {
				memcpy(test, c->board, sizeof(test));
			    } else {
				mazema = 1;
			    }
			} else {
			       /*-- WHITE --*/
			    for (p = 24, wmin = 24; p > 0; p--) {
				if (test[p].wpop) {
				    wmin = p;
				}
			    }
			    if (wmin < 19) {
				memcpy(test, c->board, sizeof(test));
			    } else {
				mazema = 1;
			    }
			}

		    }
		    i++;
		}
	    }
	    if (strlen(dice) > 2) {
		zaries2 = (dcount[0] + dcount[1] + dcount[2] + dcount[3]);
		dcount[0] = dcount[1] = dcount[2] = dcount[3] = 0;
	    } else {
		for (i = 0; i < 4; i++) {
		    if (dcount[i]) {
			dcount[i] = 0;
			zaries2++;
		    }
		}
	    }
	    zaries2 = zaries2 > strlen(dice) ? strlen(dice) : zaries2;
	    zaries = zaries2 > zaries ? zaries2 : zaries;
	}
	if (zaries < strlen(dice) && strlen(dice) == 2) {

		/*--swap dice and try again --*/
	    char d;
	    int zaries2 = 0;
	    d = c->dice[0];
	    c->dice[0] = c->dice[1];
	    c->dice[1] = d;

		/*-- recursive call with swaped dice --*/
	    if (recursion) {
		zaries2 = cmtoex(c, 0);
		return zaries > zaries2 ? zaries : zaries2;
	    }
	}
	return zaries;
    case FEYGA:
	i = 0;
/*-- feyga start rules exception check --*/
	if (test[24].bpop == 14 && color == BLACK && dice[0] != dice[1]) {
	    short ret = 0;
	    bmax = 0;
	    for (i = 23; i > 12; i--) {
		if (test[i].bpop) {
		    bmax = i;
		    break;
		}
	    }
	    if (bmax) {
		if ((dest = domove(test, bmax, dice[0], color, c->game)) > -1) {
		    if (dest < 13) {
			if (domove(test, dest, dice[1], color, c->game) > -1 || domove(test, 24, dice[1], color, c->game) > -1) {
			    return (2);
			} else {
			    ret = 1;
			}
		    } else if ((dest = domove(test, dest, dice[1], color, c->game)) > -1) {
			return (2);
		    } else {
			ret = 1;
		    }
		}
		memcpy(test, c->board, sizeof(test));
		if ((dest = domove(test, bmax, dice[1], color, c->game)) > -1) {
		    if (dest < 13) {
			if (domove(test, dest, dice[0], color, c->game) > -1 || domove(test, 24, dice[0], color, c->game) > -1) {
			    return (2);
			} else {
			    ret = 1;
			}
		    } else if ((dest = domove(test, dest, dice[0], color, c->game)) > -1) {
			return (2);
		    } else {
			ret = 1;
		    }
		}
		return (ret);
	    }
	}
	if (test[12].wpop == 14 && color == WHITE && dice[0] != dice[1]) {
	    short ret = 0;
	    wmax = 0;
	    for (i = 11; i > 0; i--) {
		if (test[i].wpop) {
		    wmax = i;
		    break;
		}
	    }
	    if (wmax) {
		if ((dest = domove(test, wmax, dice[0], color, c->game)) > -1) {
		    if (dest > 12) {
			if (domove(test, dest, dice[1], color, c->game) > -1 || domove(test, 12, dice[1], color, c->game) > -1) {
			    return (2);
			} else {
			    ret = 1;
			}
		    } else if ((dest = domove(test, dest, dice[1], color, c->game)) > -1) {
			return (2);
		    } else {
			ret = 1;
		    }
		    ret = 1;
		}
		memcpy(test, c->board, sizeof(test));
		if ((dest = domove(test, wmax, dice[1], color, c->game)) > -1) {
		    if (dest > 12) {
			if (domove(test, dest, dice[0], color, c->game) > -1 || domove(test, 12, dice[0], color, c->game) > -1) {
			    return (2);
			} else {
			    ret = 1;
			}
		    } else if ((dest = domove(test, dest, dice[0], color, c->game)) > -1) {
			return (2);
		    } else {
			ret = 1;
		    }
		    ret = 1;
		}
		return (ret);
	    }
	}
	if (test[24].bpop == 15 && color == BLACK && dice[0] != dice[1]) {
	    if ((dest = domove(test, 24, dice[0], color, c->game)) != -1) {
		dest = domove(test, dest, dice[1], color, c->game);
		if (dest != -1) {
		    return (2);
		}
		return (1);
	    }
	    if ((dest = domove(test, 24, dice[1], color, c->game)) != -1) {
		dest = domove(test, dest, dice[0], color, c->game);
		if (dest != -1) {
		    return (2);
		}
		return (1);
	    }
	    return (0);
	}

	if (test[12].wpop == 15 && color == WHITE && dice[0] != dice[1]) {
	    if ((dest = domove(test, 12, dice[0], color, c->game)) != -1) {
		dest = domove(test, dest, dice[1], color, c->game);
		if (dest != -1) {
		    return (2);
		}
		return (1);
	    }
	    if ((dest = domove(test, 12, dice[1], color, c->game)) != -1) {
		dest = domove(test, dest, dice[0], color, c->game);
		if (dest != -1) {
		    return (2);
		}
		return (1);
	    }
	    return (0);
	}

/*-- end of exception check ---*/

	mazema = 0;
	for (j = 1; j < 25; j++) {
	    s = c->turn == c->blackpos ? test[j].bpop : test[j].wpop;
	    if (!s)
		continue;
	    if (!mazema || s > 1) {
		i = 0;
	    } else {
		i = dcount[0] > dcount[1] ? 1 : 0;
	    }
	    while (s && dice[i]) {
		if ((dest = domove(test, j, dice[i], color, c->game)) > -1) {
		    int r = 1;
		    s--;
		    while (dice[i + r]) {
			if (dest != 0 && dest != 25 && (dest = domove(test, dest, dice[i + r], color, c->game)) > -1) {
			    dcount[i + r]++;
			}
			r++;
		    }
		    dcount[i]++;
		}
		if (color == BLACK) {
		    if (test[24].bpop < 14) {
			if (dest != 0 && dice[0] != dice[2]) {
			    for (p = 1, bmax = 0; p < 25; p++) {
				if (test[p].bpop) {
				    bmax = p;
				}
			    }
			    if (bmax > 6) {
				memcpy(test, c->board, sizeof(test));
			    } else {
				mazema = 1;
			    }
			}
		    }
		} else { /*-- WHITE --*/
		    if (test[12].wpop < 14) {
			if (dest != 25 && dice[0] != dice[2]) {
			    for (p = 1, wmax = 0; p < 25; p++) {
				if (test[p].wpop) {
				    wmax = p;
				}
			    }
			    for (p = 24, wmin = 24; p > 0; p--) {
				if (test[p].wpop) {
				    wmin = p;
				}
			    }
			    if (wmax > 18 || wmin < 13) {
				memcpy(test, c->board, sizeof(test));
			    } else {
				mazema = 1;
			    }
			}
		    }
		}
		i++;
	    }
	}
	if (strlen(dice) == 4) {
	    zaries = dcount[0] + dcount[1] + dcount[2] + dcount[3];
	    dcount[0] = dcount[1] = dcount[2] = dcount[3] = 0;
	} else {
	    for (i = 0; i < 4; i++) {
		if (dcount[i]) {
		    dcount[i] = 0;
		    zaries++;
		}
	    }
	}
	zaries = zaries > strlen(dice) ? strlen(dice) : zaries;

/*--- Rorate tavliera otherwise. Sometimes usefull on mazema --*/
	if (zaries < strlen(dice)) {
	    int zaries2 = 0;
	    memcpy(test, c->board, sizeof(test));
	    bzero(dcount, sizeof(dcount));
	    mazema = 0;
	    for (j = 24; j > 0; j--) {
		s = c->turn == c->blackpos ? test[j].bpop : test[j].wpop;
		if (!s)
		    continue;
		if (!mazema || s > 1) {
		    i = 0;
		} else {
		    i = dcount[0] > dcount[1] ? 1 : 0;
		}
		while (s && dice[i]) {
		    if ((dest = domove(test, j, dice[i], color, c->game)) > -1) {
			int r = 1;
			s--;
			while (dice[i + r]) {
			    if (dest != 0 && dest != 25 && (dest = domove(test, dest, dice[i + r], color, c->game)) > -1) {
				dcount[i + r]++;
			    }
			    r++;
			}
			dcount[i]++;
		    }
		    if (color == BLACK) {
			if (test[24].bpop < 14) {
			    if (dest != 0 && dice[0] != dice[2]) {
				for (p = 1, bmax = 0; p < 25; p++) {
				    if (test[p].bpop) {
					bmax = p;
				    }
				}
				if (bmax > 6) {
				    memcpy(test, c->board, sizeof(test));
				} else {
				    mazema = 1;
				}
			    }
			}
		    } else {
			 /*-- WHITE --*/
			if (test[12].wpop < 14) {
			    if (dest != 25 && dice[0] != dice[2]) {
				for (p = 1, wmax = 0; p < 25; p++) {
				    if (test[p].wpop) {
					wmax = p;
				    }
				}
				for (p = 24, wmin = 24; p > 0; p--) {
				    if (test[p].wpop) {
					wmin = p;
				    }
				}
				if (wmax > 18 || wmin < 13) {
				    memcpy(test, c->board, sizeof(test));
				} else {
				    mazema = 1;
				}
			    }
			}
		    }

		    i++;
		}
	    }
	    if (strlen(dice) > 2) {
		zaries2 = (dcount[0] + dcount[1] + dcount[2] + dcount[3]);
		dcount[0] = dcount[1] = dcount[2] = dcount[3] = 0;
	    } else {
		for (i = 0; i < 4; i++) {
		    if (dcount[i]) {
			dcount[i] = 0;
			zaries2++;
		    }
		}
	    }
	    zaries2 = zaries2 > strlen(dice) ? strlen(dice) : zaries2;
	    zaries = zaries2 > zaries ? zaries2 : zaries;
	}
	if (zaries < strlen(dice) && strlen(dice) == 2) {

		/*--swap dice and try again --*/
	    char d;
	    int zaries2 = 0;
	    d = c->dice[0];
	    c->dice[0] = c->dice[1];
	    c->dice[1] = d;

		/*-- recursive call with swaped dice --*/
	    if (recursion) {
		zaries2 = cmtoex(c, 0);
		return zaries > zaries2 ? zaries : zaries2;
	    }
	}
	return zaries;
    }
    return (-1);
		/*-- This means an error --*/
}
void draw_board(chan * c)
{

#include "bareboard.h"
    extern client *clients[];

		 /*--positions map in the ascii board (horiz axis)--*/
    short hmap[26] = { 1, 5, 8, 11, 14, 17, 20, 24, 27, 30, 33, 36, 39, 39, 36, 33, 30,
	27, 24, 20, 17, 14, 11, 8, 5, 1
    };

		/*--   ''      maps   ''  ''   (upper vert axis)--*/
    short tvmap[17] = { 0, 2, 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5, 6, 7, 8, 9
    };
    short tvpmap[17] = { 0, 3, 4, 5, 6, 7, 8, 9, 10, 3, 4, 5, 6, 7, 8, 9, 10
    };

		/*--   ''      ''           ''   ''  (lower vert axis)--*/
    short lvmap[17] = { 0, 18, 17, 16, 15, 14, 13, 12, 11, 18, 17, 16, 15, 14, 13, 12, 11
    };
    short lvpmap[17] = { 0, 17, 16, 15, 14, 13, 12, 11, 10, 17, 16, 15, 14, 13, 12, 11, 10
    };
    int i;
    col temp[26];
    char dest[BOARD_SIZE], *gm;
    memcpy(temp, c->board, sizeof(temp));
    bzero(c->asciib, BOARD_SIZE);
    bzero(dest, BOARD_SIZE);
    gm = NULL;
    for (i = 0; i < sizeof(bareboard) / sizeof(*bareboard); i++) {
	strcat(dest, bareboard[i]);
	strcat(dest, "\r\n");
    }
    for (i = 0; i <= 12; i++) {
	if (temp[i].top == WHITE) {
	    while (temp[i].wpop) {
		dest[((temp[i].bpop > 0 ? tvpmap[temp[i].wpop] : tvmap[temp[i].wpop]) * 48) + (hmap[i]) - (temp[i].wpop > 8 ? 1 : 0)] = 'o';
		temp[i].wpop--;
	    }
	    while (temp[i].bpop) {
		dest[(tvmap[temp[i].bpop] * 48) + (hmap[i]) - (temp[i].bpop > 8 ? 1 : 0)] = '*';
		temp[i].bpop--;
	    }
	} else {
	    while (temp[i].bpop) {
		dest[((temp[i].wpop > 0 ? tvpmap[temp[i].bpop] : tvmap[temp[i].bpop]) * 48) + (hmap[i]) - (temp[i].bpop > 8 ? 1 : 0)] = '*';
		temp[i].bpop--;
	    }
	    while (temp[i].wpop) {
		dest[(tvmap[temp[i].wpop] * 48) + (hmap[i]) - (temp[i].wpop > 8 ? 1 : 0)] = 'o';
		temp[i].wpop--;
	    }
	}
    }
    for (i = 13; i <= 25; i++) {
	if (temp[i].top == WHITE) {
	    while (temp[i].wpop) {
		dest[((temp[i].bpop > 0 ? lvpmap[temp[i].wpop] : lvmap[temp[i].wpop]) * 48) + (hmap[i]) - (temp[i].wpop > 8 ? 1 : 0)] = 'o';
		temp[i].wpop--;
	    }
	    while (temp[i].bpop) {
		dest[(lvmap[temp[i].bpop] * 48) + (hmap[i]) - (temp[i].bpop > 8 ? 1 : 0)] = '*';
		temp[i].bpop--;
	    }
	} else {
	    while (temp[i].bpop) {
		dest[((temp[i].wpop > 0 ? lvpmap[temp[i].bpop] : lvmap[temp[i].bpop]) * 48) + (hmap[i]) - (temp[i].bpop > 8 ? 1 : 0)] = '*';
		temp[i].bpop--;
	    }
	    while (temp[i].wpop) {
		dest[(lvmap[temp[i].wpop] * 48) + (hmap[i]) - (temp[i].wpop > 8 ? 1 : 0)] = 'o';
		temp[i].wpop--;
	    }
	}
    }
    switch (c->game) {
    case PORTES:
	gm = "PORTES";
	break;
    case PLAKWTO:
	gm = "PLAKWTO";
	break;
    case FEYGA:
	gm = "FEYGA";
    }
    sprintf(c->asciib, dest, "%s (white o)", "%s (black *)", "Score: %d-%d", "%s");
    sprintf(dest, c->asciib, clients[c->whitepos]->nick, clients[c->blackpos]->nick, c->score_w, c->score_bl, gm);
    strcpy(c->asciib, "\r\n");
    strcat(c->asciib, dest);
}
