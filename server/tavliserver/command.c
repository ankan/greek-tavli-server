#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#define _XOPEN_SOURCE
#include <unistd.h>
#include <syslog.h>
#include "config.h"
#include "prototypes.h"
#include "tavtypes.h"
#include "messages.h"
#include "commands.h"
extern client *clients[];
extern unsigned short clicnt;
extern char outbuf[];
int proc_cmd(int fd, int pos)
{
    char buf[BUFSIZ * 4], c, prev, cmd[16], arg[40 + 1], rest[MAX_UINPUT - 57];
    int i, j, k, firstalpha = 0;
    bzero(buf, BUFSIZ * 4);
    bzero(cmd, sizeof(cmd));
    bzero(arg, sizeof(arg));
    bzero(rest, sizeof(rest));

/*-- Spaces at start and duplicated spaces away ----*/
    for (j = 0, k = 0, prev = 'a'; clients[pos]->cmdbuf[j] != 0; j++) {
	c = clients[pos]->cmdbuf[j];
	if (isprint(c) && !isspace(c)) {
	    firstalpha = 1;
	} else {
	    c = 32;   /*-- tabs away --*/
	}
	if (!firstalpha || !isprint(c) || (isspace(c) && isspace(prev)) || c == CR || c == LF) {
	    ;
	} else {
	    buf[k++] = c;
	}
	prev = c;
    }
    bzero(clients[pos]->cmdbuf, BUFSIZ);
    if (strlen(buf) == 0) {
	j = response(fd, pos, NULL);

			    /*--nothing to do, just type the prompt--*/
	return (j);
    }
    if (buf[0] != '/') {
      /*-- This is a message to channel. --*/
	return speak_to_chan(pos, buf);
    }

/*--- split the command into cmd, arg & rest -----*/
/*--- strtok() sucks, so I do it the hard way :-( --*/
    j = 1;
	    /*-- leading / away --*/
    k = 0;
    i = strlen(buf);
    while (buf[j] != ' ' && j <= i && k < 16) {
	cmd[k++] = buf[j++];
    }
    k = 0;
    if (buf[j] == ' ' && strlen(cmd)) {
	while (buf[++j] != ' ' && j < i && k < 40) {
	    arg[k++] = buf[j];
	}
    } else {
	response(fd, pos, MSG_NO_CMD);
	return (0);
    }
    k = 0;
    if (buf[j] == ' ') {
	j++;
	while (j < i && k < 140) {
	    rest[k++] = buf[j++];
	}
    } else {
	if (strlen(arg)) {
	    sprintf(rest, MSG_ARG_BIG, cmd);
	    response(fd, pos, rest);
	    return (0);
	}
    }
    bzero(buf, BUFSIZ * 4);

/*----------------------------------------------------------
 *--- Command is broken into cmd, arg & rest 
 *--- It's the right time for the big boring "if" sequence 
 *---------------------------------------------------------*/
#define eq(x,y)  !strcmp(x,y)
    if (eq(cmd, "help") || eq(cmd, "h") || eq(cmd, "?")) {
	return do_help(fd, pos, arg);
    }
    if (eq(cmd, "msg")) {
	return msg_user(pos, arg, rest);
    }
    if (eq(cmd, "silence")) {
	char *msg;
	if ((clients[pos]->status & SILENCE) == SILENCE) {
	    msg = MSG_SILENCE_OFF;
	} else {
	    msg = MSG_SILENCE_ON;
	}
	clients[pos]->status ^= SILENCE;
	return response(fd, pos, msg);
    }
    if (eq(cmd, "count")) {
	sprintf(buf, MSG_COUNT, clicnt);
	return response(fd, pos, buf);
    }
    if (eq(cmd, "invite") || eq(cmd, "inv")) {
	int points;
	short startgame = PORTES;
	if (!strlen(arg)) {
	    return response(fd, pos, USG_INVITE);
	}
	if (!strlen(rest)) {
	    clients[pos]->inv_pts = WINSCORE;
	} else if (strstr(rest, "portes") != NULL) {
	    clients[pos]->inv_pts = 1;
	} else if (strstr(rest, "plakwto") != NULL) {
	    clients[pos]->inv_pts = 1;
	    startgame = PLAKWTO;
	} else if (strstr(rest, "feyga") != NULL) {
	    clients[pos]->inv_pts = 1;
	    startgame = FEYGA;
	} else {
	    points = strtol(rest, (char **) NULL, 10);
	    switch (points) {
	    case 3:
		clients[pos]->inv_pts = 3;
		break;
	    case 5:
		clients[pos]->inv_pts = 5;
		break;
	    case 7:
		clients[pos]->inv_pts = 7;
		break;
	    default:
		clients[pos]->inv_pts = WINSCORE;
	    }
	}

	return invite(pos, arg, clients[pos]->inv_pts, startgame);
    }
    if (eq(cmd, "uninvite") || eq(cmd, "uninv")) {
	clients[pos]->inv_pos = -1;
	return response(fd, pos, MSG_CANCEL_INV);
    }
    if (eq(cmd, "accept") || eq(cmd, "acc")) {
	if (!strlen(arg)) {
	    return response(fd, pos, USG_ACCEPT);
	}
	return inv_accept(pos, arg);
    }
    if (eq(cmd, "watch")) {
	int found = 0;
	extern chan *chans[];
	if (!strlen(arg)) {
	    return response(fd, pos, USG_WATCH);
	}
	if (clients[pos]->chan != -1) {
	    return response(fd, pos, MSG_ALLREADY_WATCH);
	}
	for (j = 0; j < MAX_CLIENTS; j++) {
	    if (!strcmp(clients[j]->nick, arg)) {
		found++;
		break;
	    }
	}
	if (!found) {
	    sprintf(buf, MSG_NO_NICK, arg);
	    return response(fd, pos, buf);
	}
	if ((clients[j]->status & PLAYER) != PLAYER) {
	    sprintf(buf, MSG_HE_NOT_PLAYER, arg);
	    return response(fd, pos, buf);
	}
	if ((chans[clients[j]->chan]->status & PRIVE) == PRIVE) {
	    sprintf(buf, MSG_PRIVE, arg);
	    return response(fd, pos, buf);
	}
	for (i = 0; i < MAX_BANNED; i++) {
	    if (chans[clients[j]->chan]->ban[i] == pos) {
		return response(fd, pos, MSG_YOU_BANNED);
	    }
	}
	sprintf(buf, MSG_NEW_WATCH, clients[pos]->nick);
	send_to_chan(j, 0, buf);
	clients[pos]->chan = clients[j]->chan;
	if ((clients[pos]->status & GUI) == GUI) {
	    snprintf(buf, BUFSIZ - 2, GUI_CHAN_W, clients[chans[clients[pos]->chan]->blackpos]->nick, chans[clients[pos]->chan]->score_bl, clients[chans[clients[pos]->chan]->whitepos]->nick, chans[clients[pos]->chan]->score_w, chans[clients[pos]->chan]->dice[0], chans[clients[pos]->chan]->dice[1], chans[clients[pos]->chan]->turn == chans[clients[pos]->chan]->blackpos ? BLACK : WHITE, chans[clients[pos]->chan]->game);
	    strcat(buf, "\r\n");
	    bzero(outbuf, BUFSIZ);
	    strcat(buf, GUI_POSITIONS);
	    for (i = 0; i < 26; i++) {

			/*-- position bpop wpop top#....E --*/
		sprintf(outbuf, "%d %d %d %d#", i, chans[clients[pos]->chan]->board[i].bpop, chans[clients[pos]->chan]->board[i].wpop, chans[clients[pos]->chan]->board[i].top);
		strncat(buf, outbuf, BUFSIZ - strlen(buf) - 2);
	    }
	    strcat(buf, "E");
	    return response(fd, pos, buf);

	    /* TODO: more GUI stuff */
	} else {
	    return response(fd, pos, chans[clients[pos]->chan]->asciibout);
	}
	return response(fd, pos, NULL);

				      /*-- prompt back --*/
    }
    if (eq(cmd, "part")) {
	if (clients[pos]->chan < 0) {
	    return response(fd, pos, MSG_NO_CHAN);
	}
	if ((clients[pos]->status & PLAYER) == PLAYER) {
	    return response(fd, pos, MSG_PLAYER_CANT_PART);
	}
	sprintf(buf, MSG_LEFT_CHAN, clients[pos]->nick);
	send_to_chan(pos, 0, buf);
	if ((clients[pos]->status & GUI) == GUI) {
	    response(fd, pos, GUI_NOCHAN);
	}
	clients[pos]->chan = -1;
	bzero(buf, BUFSIZ);
	return (0);
    }
    if (eq(cmd, "prive")) {
	extern chan *chans[];
	if ((clients[pos]->status & PLAYER) != PLAYER) {
	    return response(fd, pos, MSG_NO_PLAYER);
	}
	if ((chans[clients[pos]->chan]->status & PRIVE) == PRIVE) {
	    chans[clients[pos]->chan]->status &= ~PRIVE;
	    sprintf(buf, MSG_STOP_PRIVE, clients[pos]->nick);
	    return send_to_chan(pos, 1, buf);
	}
	k = chans[clients[pos]->chan]->blackpos;
	i = chans[clients[pos]->chan]->whitepos;
	chans[clients[pos]->chan]->status |= PRIVE;
	sprintf(buf, MSG_GO_PRIVE, clients[pos]->nick);
	send_to_chan(pos, 1, buf);
	for (j = 0; j < MAX_CLIENTS; j++) {
	    if (clients[j]->chan == clients[pos]->chan && j != k && j != i) {
		clients[j]->chan = -1;
		if ((clients[j]->status & GUI) == GUI) {
		    response(clients[j]->fd, j, GUI_NOCHAN);
		}
	    }
	}
	return (0);
    }

    if (eq(cmd, "quiet")) {
	extern chan *chans[];
	if ((clients[pos]->status & PLAYER) != PLAYER) {
	    return response(fd, pos, MSG_NO_PLAYER);
	}
	bzero(outbuf, BUFSIZ);

	if ((clients[pos]->status & BLACK) == BLACK) {
	    if ((chans[clients[pos]->chan]->status & QUIET_WHITE) == QUIET_WHITE) {
		return response(fd, pos, MSG_ALLREADY_QUIET);
	    } else if ((chans[clients[pos]->chan]->status & QUIET_BLACK) == QUIET_BLACK) {
		sprintf(outbuf, MSG_UNSET_QUIET, clients[pos]->nick);
	    } else {
		sprintf(outbuf, MSG_SET_QUIET, clients[pos]->nick);
	    }
	    chans[clients[pos]->chan]->status ^= QUIET_BLACK;
	    send_to_chan(pos, 1, outbuf);
	} else { /*-- WHITE player --*/
	    if ((chans[clients[pos]->chan]->status & QUIET_BLACK) == QUIET_BLACK) {
		return response(fd, pos, MSG_ALLREADY_QUIET);
	    } else if ((chans[clients[pos]->chan]->status & QUIET_WHITE) == QUIET_WHITE) {
		sprintf(outbuf, MSG_UNSET_QUIET, clients[pos]->nick);
	    } else {
		sprintf(outbuf, MSG_SET_QUIET, clients[pos]->nick);
	    }
	    chans[clients[pos]->chan]->status ^= QUIET_WHITE;
	    send_to_chan(pos, 1, outbuf);
	}
	return (0);

    }
    if (eq(cmd, "kick")) {
	int found = 0;
	if ((clients[pos]->status & PLAYER) != PLAYER) {
	    return response(fd, pos, MSG_NO_PLAYER);
	}
	if (!strlen(arg)) {
	    return response(fd, pos, USG_KICK);
	}
	for (j = 0; j < MAX_CLIENTS; j++) {
	    if (clients[j]->chan == clients[pos]->chan && !strcmp(clients[j]->nick, arg)) {
		found++;
		break;
	    }
	}
	if (!found) {
	    sprintf(buf, MSG_NO_NICK_ON_CHAN, arg);
	    return response(fd, pos, buf);
	}
	if (j == pos) {
	    return response(fd, pos, MSG_KICK_YOURSELF);
	}
	if ((clients[j]->status & PLAYER) == PLAYER) {
	    return response(fd, pos, MSG_KICK_OPP);
	}
	clients[j]->chan = -1;
	if (!strlen(rest)) {
	    sprintf(buf, MSG_KICK, clients[pos]->nick, arg);
	} else {
	    sprintf(buf, MSG_KICK_REASON, clients[pos]->nick, arg, rest);
	}
	send_to_chan(pos, 0, buf);
	clients[j]->chan = -1;
	if ((clients[j]->status & GUI) == GUI) {
	    response(clients[j]->fd, j, GUI_NOCHAN);
	}
	return (0);
    }
    if (eq(cmd, "mute")) {
	int found = 0;
	if ((clients[pos]->status & ISOP) != ISOP) {
	    return response(fd, pos, MSG_NO_OP);
	}
	if (!strlen(arg)) {
	    return response(fd, pos, USG_MUTE);
	}
	for (j = 0; j < MAX_CLIENTS; j++) {
	    if (!strcmp(clients[j]->nick, arg)) {
		found++;
		break;
	    }
	}
	if (!found) {
	    sprintf(buf, MSG_NO_NICK, arg);
	    return response(fd, pos, buf);
	}
	if ((clients[j]->status & MUTED) != MUTED) {
	    clients[j]->status |= MUTED;
	    sprintf(buf, "*** %s muted", arg);
	    return response(fd, pos, buf);
	} else {
	    clients[j]->status &= ~(MUTED);
	    sprintf(buf, "*** %s demuted", arg);
	    return response(fd, pos, buf);
	}
    }

    if (eq(cmd, "op")) {
        int found = 0;
        if ((clients[pos]->status & HARD_OP) != HARD_OP) {
            return response(fd, pos, MSG_NO_HARDOP);
        }
        if (!strlen(arg)) {
            return response(fd, pos, USG_OP);
        }
        for (j = 0; j < MAX_CLIENTS; j++) {
            if (!strcmp(clients[j]->nick, arg)) {
                found++;
                break;
            }
        }
        if (!found) {
            sprintf(buf, MSG_NO_NICK, arg);
            return response(fd, pos, buf);
        }
        if ((clients[j]->status & ISOP) != ISOP) {
            clients[j]->status |= ISOP;
            sprintf(buf, "*** %s is an operator", arg);
            return response(fd, pos, buf);
        } else {
            clients[j]->status &= ~(ISOP);
            sprintf(buf, "*** %s is no more an operator", arg);
            return response(fd, pos, buf);
        }
    }


    if (eq(cmd, "quit") || eq(cmd, "exit")) {
	if ((clients[pos]->status & PLAYER) == PLAYER) {
	    return response(fd, pos, MSG_PLAYER_QUIT);
	}
	if (clients[pos]->chan != -1) {
	    sprintf(buf, MSG_QUITED, clients[pos]->nick);
	    send_to_chan(pos, 1, buf);
	}
	bye_client(clients[pos]->fd);
	return (0);
    }
    if (eq(cmd, "whois")) {
	int found = 0;
	if (!strlen(arg)) {
	    return response(fd, pos, USG_WHOIS);
	}
	for (j = 0; j < MAX_CLIENTS; j++) {
	    if (!strcmp(arg, clients[j]->nick)) {
		found++;
		break;
	    }
	}
	if (!found) {
	    bzero(outbuf, BUFSIZ);
	    sprintf(outbuf, MSG_NO_NICK, arg);
	    return response(fd, pos, outbuf);
	}
	if (!strlen(clients[j]->name)) {
	    sprintf(buf, MSG_WHOIS_NONAME, arg, clients[j]->addr, ctime(&clients[j]->contime));
	} else {
	    sprintf(buf, MSG_WHOIS, arg, clients[j]->name, clients[j]->addr, ctime(&clients[j]->contime));
	}
	return response(fd, pos, buf);
    }
    if (eq(cmd, "name")) {
	if (!strlen(arg)) {
	    return response(fd, pos, USG_NAME);
	}
	sprintf(buf, "%s ", arg);
	if (strlen(rest)) {
	    strcat(buf, rest);
	}
	sprintf(clients[pos]->name, "%s", buf);
	return response(fd, pos, NULL);	     /*-- prompt back --*/
    }
    if (eq(cmd, "ban")) {
	int found = 0;
	extern chan *chans[];
	if (!strlen(arg)) {
	    return response(fd, pos, USG_BAN);
	}
	if ((clients[pos]->status & PLAYER) != PLAYER) {
	    return response(fd, pos, MSG_NO_PLAYER);
	}
	for (i = 0; i < MAX_CLIENTS; i++) {
	    if (!strcmp(arg, clients[i]->nick)) {
		found++;
		break;
	    }
	}
	if (!found) {
	    sprintf(buf, MSG_NO_NICK, arg);
	    return response(fd, pos, buf);
	}
	if (i == pos) {
	    return response(fd, pos, MSG_BAN_YOURSELF);
	}
	if (chans[clients[pos]->chan]->blackpos == i || chans[clients[pos]->chan]->whitepos == i) {
	    return response(fd, pos, MSG_BAN_OPPO);
	}
	found = 0;
	for (j = 0; j < MAX_BANNED; j++) {
	    if (chans[clients[pos]->chan]->ban[j] == i) {
		sprintf(buf, MSG_ALLREADY_BAN, clients[i]->nick);
		return response(fd, pos, buf);
	    }
	}
	for (j = 0; j < MAX_BANNED; j++) {
	    if (chans[clients[pos]->chan]->ban[j] == -1) {
		chans[clients[pos]->chan]->ban[j] = i;
		found++;
		break;
	    }
	}
	if (!found) {
	    return response(fd, pos, MSG_MANY_BAN);
	}
	sprintf(buf, MSG_BAN, arg, clients[pos]->nick);
	send_to_chan(pos, 0, buf);
	if (clients[i]->chan == clients[pos]->chan) {
	    clients[i]->chan = -1;
	    if ((clients[i]->status & GUI) == GUI) {
		response(clients[i]->fd, i, GUI_NOCHAN);
	    }
	}
	return (0);
    }

    if (eq(cmd, "ignore")) {
	int found = 0;
	if (!strlen(arg)) {
	    return response(fd, pos, USG_IGNORE);
	}
	for (i = 0; i < MAX_CLIENTS; i++) {
	    if (!strcmp(arg, clients[i]->nick)) {
		found++;
		break;
	    }
	}
	if (!found) {
	    sprintf(buf, MSG_NO_NICK, arg);
	    return response(fd, pos, buf);
	}
	if (i == pos) {
	    return response(fd, pos, MSG_IGNORE_YOURSELF);
	}

	found = 0;
	for (j = 0; j < MAX_IGNORED; j++) {
	    if (clients[pos]->ignored[j] == i) {
		if (clients[pos]->ignored_id[j] == clients[i]->rand_id) {
		    sprintf(buf, MSG_ALLREADY_IGNORED, clients[i]->nick);
		    return response(fd, pos, buf);
		} else {
		    clients[pos]->ignored[j] = -1;
		}
	    }
	}
	for (j = 0; j < MAX_IGNORED; j++) {
	    if (clients[pos]->ignored[j] == -1) {
		clients[pos]->ignored[j] = i;
		clients[pos]->ignored_id[j] = clients[i]->rand_id;
		found++;
		break;
	    }
	}
	if (!found) {
	    return response(fd, pos, MSG_MANY_IGNORED);
	}
	sprintf(buf, MSG_IGNORE_OK, clients[i]->nick);
	return response(fd, pos, buf);
    }


    if (eq(cmd, "names")) {
	extern chan *chans[];
	if (clients[pos]->chan == -1) {
	    return response(fd, pos, MSG_NO_CHAN);
	}
	strcat(buf, TOKEN_NAMES);
	j = chans[clients[pos]->chan]->blackpos;
	k = chans[clients[pos]->chan]->whitepos;
	sprintf(rest, " %s", clients[j]->nick);
	strcat(buf, rest);
	sprintf(rest, " %s", clients[k]->nick);
	strcat(buf, rest);
	for (i = 0; i < MAX_CLIENTS; i++) {
	    if (clients[i]->chan == clients[pos]->chan && i != j && i != k) {
		sprintf(rest, " %s", clients[i]->nick);
		strncat(buf, rest, BUFSIZ - (strlen(buf) + 2));
	    }
	}
	return response(fd, pos, buf);
    }
    if (eq(cmd, "games")) {
	extern chan *chans[];
	char whobuf[BUFSIZ * 4];
	k = 0;
	bzero(whobuf, BUFSIZ * 4);
	for (i = 0; i < MAX_CHANS; i++) {
	    if (chans[i]->game == -1) {
		continue;
	    }
	    sprintf(buf, "%s %s vs %s %d-%d (%d)\r\n", TOKEN_GAMES, clients[chans[i]->blackpos]->nick, clients[chans[i]->whitepos]->nick, chans[i]->score_bl, chans[i]->score_w, chans[i]->winscore);
	    strncat(whobuf, buf, (BUFSIZ * 4) - (strlen(whobuf) + 5));
	    k++;
	}
	if (!k) {
	    return response(fd, pos, MSG_NO_GAMES);
	}
	return response(fd, pos, whobuf);

				      /*-- prompt back --*/
    }
    if (eq(cmd, "who")) {
	char whobuf[BUFSIZ * 4];
	bzero(whobuf, BUFSIZ * 4);
	j = 0;
	for (i = 0; i < MAX_CLIENTS; i++) {
	    if ((clients[i]->status & FREE_TO_PLAY) == 0) {
		continue;
	    }
	    j++;
	    if ((clients[i]->status & ISOP) != ISOP) {
		sprintf(buf, "%s %s\r\n", TOKEN_FREE, clients[i]->nick);
	    } else {
		sprintf(buf, "%s %s @\r\n", TOKEN_FREE, clients[i]->nick);
	    }
	    strncat(whobuf, buf, (BUFSIZ * 4) - (strlen(whobuf) + 5));
	}
	if (!j) {
	    return response(fd, pos, MSG_NOB_WAITS);
	}
	return response(fd, pos, whobuf);

				      /*--prompt back --*/
    }
    if (eq(cmd, "free")) {
	if ((clients[pos]->status & FREE_TO_PLAY) == FREE_TO_PLAY) {
	    response(fd, pos, MSG_SET_NOFREE);
	} else {
	    response(fd, pos, MSG_SET_FREE);
	}
	clients[pos]->status ^= FREE_TO_PLAY;
	for (i = 0; i < MAX_CLIENTS; i++) {
	    if ((clients[i]->status & GUI) == GUI) {
		response(clients[i]->fd, i, GUI_SEND_WHO);
	    }
	}
	return (0);
    }
    if (eq(cmd, "user")) {
	if (!strlen(arg)) {
	    return response(fd, pos, USG_USER);
	}
	for (i = 0; i < MAX_CLIENTS; i++) {
	    if (!strcmp(arg, clients[i]->nick)) {
		return response(fd, pos, MSG_DOUBL_LOG_IN);
	    }
	    if (!strcmp(clients[i]->addr, clients[pos]->addr) && (clients[i]->status & REGISTERED) == REGISTERED) {
		return response(fd, pos, MSG_SAME_IP_LOGIN);
	    }
	}
	if ((clients[pos]->status & PLAYER) == PLAYER) {
	    return response(fd, pos, MSG_PLAYER_LOG_IN);
	}
	strncpy(clients[pos]->regnick, arg, 40 - 1);
	sprintf(buf, MSG_USER, clients[pos]->regnick);
	return response(fd, pos, buf);
    }
    if (eq(cmd, "pass")) {
	FILE *pwf;
	char *nick, *encpass, *isop, *res, s[5];
	k = 0;
	if (!strlen(clients[pos]->regnick)) {
	    return response(fd, pos, MSG_PASS_NO_NICK);
	}
	if ((clients[pos]->status & PLAYER) == PLAYER) {
	    return response(fd, pos, MSG_PLAYER_LOG_IN);
	}
	if ((pwf = fopen(MY_PASS_FILE, "r")) == NULL) {
	    sprintf(buf, LOGY_ERR_PASS, MY_PASS_FILE);
	    syslog(LOG_ERR, buf);
	    return response(fd, pos, MSG_SERR_PASS);
	}
	while (fgets(buf, BUFSIZ, pwf) != NULL) {
	    nick = strtok(buf, ":");
	    if (!strcmp(nick, clients[pos]->regnick)) {
		encpass = strtok(NULL, ":");
		isop = strtok(NULL, ":");
		snprintf(rest, 3, encpass);	 /*--The salt --*/
		snprintf(s, 3, isop);
		res = crypt(arg, rest);
		if (!strcmp(encpass, res)) {
		    k++;
		}
		break;
	    }
	}
	fclose(pwf);
	if (!k) {
	    sprintf(buf, MSG_BAD_PASS, clients[pos]->regnick);
	    response(fd, pos, buf);
	    bzero(buf, BUFSIZ * 4);
	    return (0);
	}
	sprintf(clients[pos]->nick, "%s", clients[pos]->regnick);
	sprintf(buf, MSG_PASS_OK, clients[pos]->nick);
#ifdef DB_LOGGING
	login_log(clients[pos]->nick);
#endif
	clients[pos]->status |= REGISTERED;

	if (!strncmp(s, "1", 1)) {
	    clients[pos]->status |= ISOP;
	    clients[pos]->status |= HARD_OP;
	    strcat(buf, " You are an operator");
	}

	if ((clients[pos]->status & GUI) == GUI) {
	    bzero(outbuf, BUFSIZ);
	    snprintf(outbuf, BUFSIZ - 2, GUI_NICK, clients[pos]->nick);
	    strncat(buf, outbuf, BUFSIZ - 2);
	}

	response(fd, pos, buf);
	bzero(buf, BUFSIZ);
		/*-- Report the GUI clients to send the /who command --*/
	for (j = 0; j < MAX_CLIENTS; j++) {
	    if ((clients[j]->status & GUI) == GUI) {
		response(clients[j]->fd, j, GUI_SEND_WHO);
	    }
	}

	return (0);
    }
    if (eq(cmd, "board") || eq(cmd, "b")) {
	extern chan *chans[];
	if ((clients[pos]->status & GUI) == GUI) {
	    return response(fd, pos, NULL);
	}
	if (clients[pos]->chan < 0) {
	    return response(fd, pos, MSG_NO_CHAN);
	}
	if ((clients[pos]->status & GUI) != GUI) {
	    return response(fd, pos, chans[clients[pos]->chan]->asciibout);
	}
	bzero(outbuf, BUFSIZ);
	strcat(buf, GUI_POSITIONS);
	for (i = 0; i < 26; i++) {

			/*-- #position bpop wpop top#.... --*/
	    sprintf(outbuf, "#%d %d %d %d", i, chans[clients[pos]->chan]->board[i].bpop, chans[clients[pos]->chan]->board[i].wpop, chans[clients[pos]->chan]->board[i].top);
	    strncat(buf, outbuf, BUFSIZ - strlen(buf) - 2);
	}
	strncat(buf, outbuf, BUFSIZ - strlen(buf) - 2);
	return response(fd, pos, buf);
    }
    if (eq(cmd, "roll") || eq(cmd, "r")) {
	extern chan *chans[];
	if ((clients[pos]->status & PLAYER) != PLAYER) {
	    return response(fd, pos, MSG_NO_PLAYER);
	}
	if (chans[clients[pos]->chan]->turn != pos) {
	    return response(fd, pos, MSG_NO_TURN);
	}
	if ((chans[clients[pos]->chan]->status & ROLLED) == ROLLED) {
	    return response(fd, pos, MSG_ALLREADY_ROLLED);
	}
	return cmd_roll(pos);
    }
    if (eq(cmd, "move") || eq(cmd, "m") || eq(cmd, "mv")) {
	extern chan *chans[];
	char *mv[4];
	col saveb[26];
	char *cdice[4];
	char *cstart[4];
	char *nick, *opnick;
	short dice[4];
	short start[4];
	short color = 0, rotate = 0, win = 0, home = 0, ophome = 0;
	chan *c;
	short *scor;
	c = chans[clients[pos]->chan];
	if ((clients[pos]->status & PLAYER) != PLAYER) {
	    return response(fd, pos, MSG_NO_PLAYER);
	}
	if (c->turn != pos) {
	    return response(fd, pos, MSG_NO_TURN);
	}
	if ((c->status & ROLLED) != ROLLED) {
	    return response(fd, pos, MSG_FIRST_ROLL);
	}
	if (!strlen(arg)) {
	    return response(fd, pos, USG_MOVE);
	}
	sprintf(buf, "%s", arg);
	strcat(buf, rest);
	if (strlen(buf) > 38) {
	    return response(fd, pos, USG_MOVE);
	}
	bzero(rest, MAX_UINPUT - 55);
	for (i = 0, j = 0, k = 0; buf[i] != '\0'; i++) {
	    if (isspace(buf[i]) || buf[i] == CR || buf[i] == LF) {
		continue;
	    }

	/*-- the 2 following if's are there because I allways mistype --*/
	    if (buf[i] == '.') {
		buf[i] = ',';
	    }
	    if (buf[i] == '-') {
		buf[i] = '/';
	    }
	    rest[j++] = buf[i];
	    if (buf[i] == ',') {
		k++;
	    }
	}
	strncpy(c->move, rest, sizeof(c->move) - 1);
	if (k + 1 != c->mtoex) {
	    sprintf(buf, MSG_COUNT_MOVE, c->mtoex, k + 1);
	    return response(fd, pos, buf);
	}
	color = pos == c->blackpos ? BLACK : WHITE;
	firstalpha = k;
	if ((mv[0] = strtok(rest, ",")) == NULL) {
	    return response(fd, pos, USG_MOVE);
	}
	for (i = 1; k > 0; k--, i++) {
	    if ((mv[i] = strtok(NULL, ",")) == NULL) {
		return response(fd, pos, USG_MOVE);
	    }
	}
	k = firstalpha;
	for (i = 0; k + 1 > 0; k--, i++) {
	    if (strchr(mv[i], '/') == NULL) {
		sprintf(buf, MSG_INV_MOVE, mv[i]);
		return response(fd, pos, buf);
	    }
	}
	k = firstalpha;
	for (i = 0; k + 1 > 0; k--, i++) {
	    cstart[i] = strtok(mv[i], "/");
	    if (strstr(cstart[i], "ta") != NULL) {
		if (c->game != PORTES) {
		    sprintf(buf, MSG_INV_TAKOS);
		    if ((clients[pos]->status & GUI) == GUI) {
			strcat(buf, GUI_REJECT);
		    }
		    return response(fd, pos, buf);
		}
		if (color == BLACK) {
		    start[i] = 0;
		} else {
		    start[i] = 25;
		}
	    } else {
		if (cstart[i] == NULL || !strlen(cstart[i])) {
		    sprintf(buf, MSG_INV_SER_MOVE, i + 1);
		    if ((clients[pos]->status & GUI) == GUI) {
			strcat(buf, GUI_REJECT);
		    }
		    return response(fd, pos, buf);
		}
		start[i] = (short) strtol(cstart[i], NULL, 10);
		if (start[i] < 0 || start[i] > 25) {
		    sprintf(buf, MSG_INV_START, cstart[i]);
		    if ((clients[pos]->status & GUI) == GUI) {
			strcat(buf, GUI_REJECT);
		    }
		    return response(fd, pos, buf);
		}
	    }
	    cdice[i] = strtok(NULL, "/");
	    if (cdice[i] == NULL || !strlen(cdice[i])) {
		sprintf(buf, MSG_INV_SER_MOVE, i + 1);
		if ((clients[pos]->status & GUI) == GUI) {
		    strcat(buf, GUI_REJECT);
		}
		return response(fd, pos, buf);
	    }
	    dice[i] = (short) strtol(cdice[i], NULL, 10);
	    if ((dice[i] > 6 || dice[i] < 1) || strchr(c->dice, dice[i]) == NULL) {
		sprintf(buf, MSG_INV_DICE, cdice[i]);
		if ((clients[pos]->status & GUI) == GUI) {
		    strcat(buf, GUI_REJECT);
		}
		return response(fd, pos, buf);
	    }
	    if (i == 1 && dice[i] == dice[0] && strlen(c->dice) != 4) {
		sprintf(buf, MSG_INV_DICE, cdice[i]);
		if ((clients[pos]->status & GUI) == GUI) {
		    strcat(buf, GUI_REJECT);
		}
		return response(fd, pos, buf);
	    }
	}
	memcpy(saveb, c->board, sizeof(saveb));
	k = firstalpha;
	for (i = 0; k + 1 > 0; k--, i++) {
	    if (domove(saveb, start[i], dice[i], color, c->game) < 0) {
		sprintf(buf, MSG_INV_SER_MOVE, i + 1);
		if ((clients[pos]->status & GUI) == GUI) {
		    strcat(buf, GUI_REJECT);
		}
		return response(fd, pos, buf);
	    }
	}
	if (c->game == FEYGA) {
	    if (color == BLACK && saveb[19].top == BLACK && saveb[20].top == BLACK && saveb[21].top == BLACK && saveb[22].top == BLACK && saveb[23].top == BLACK && saveb[24].top == BLACK) {
		sprintf(buf, MSG_INV_MOVE, "You can't close all these");
		if ((clients[pos]->status & GUI) == GUI) {
		    strcat(buf, GUI_REJECT);
		}
		return response(fd, pos, buf);
	    }
	    if (color == WHITE && saveb[7].top == WHITE && saveb[8].top == WHITE && saveb[9].top == WHITE && saveb[10].top == WHITE && saveb[11].top == WHITE && saveb[12].top == WHITE) {
		sprintf(buf, MSG_INV_MOVE, "You can't close all these");
		if ((clients[pos]->status & GUI) == GUI) {
		    strcat(buf, GUI_REJECT);
		}
		return response(fd, pos, buf);
	    }
	    if (color == WHITE) {
		int exaporto = 0;
		for (i = 1; i < 25; i++) {
		    if (saveb[i].bpop == 16) {
			for (j = 1; j <= 6; j++) {
			    if (domove(saveb, i, j, BLACK, FEYGA) > -1) {
				exaporto++;
				break;
			    }
			}
			if (!exaporto) {
			    sprintf(buf, MSG_INV_MOVE, "You must open a door to your opponent");
			    if ((clients[pos]->status & GUI) == GUI) {
				strcat(buf, GUI_REJECT);
			    }
			    return response(fd, pos, buf);
			}
		    }
		}
	    }
	    if (color == BLACK) {
		int exaporto = 0;
		for (i = 1; i < 25; i++) {
		    if (saveb[i].wpop == 16) {
			for (j = 1; j <= 6; j++) {
			    if (domove(saveb, i, j, WHITE, FEYGA) > -1) {
				exaporto++;
				break;
			    }
			}
			if (!exaporto) {
			    sprintf(buf, MSG_INV_MOVE, "You must open a door to your opponent");
			    if ((clients[pos]->status & GUI) == GUI) {
				strcat(buf, GUI_REJECT);
			    }
			    return response(fd, pos, buf);
			}
		    }
		}
	    }
	}
	memcpy(c->board, saveb, sizeof(saveb));
	if (color == BLACK) {
	    nick = clients[c->blackpos]->nick;
	    opnick = clients[c->whitepos]->nick;
	    c->turn = c->whitepos;
	    scor = &c->score_bl;
	    switch (c->game) {
	    case PORTES:
		home = 25;
		ophome = 0;
		break;
	    case PLAKWTO:
	    case FEYGA:
		home = 0;
		ophome = 25;
		break;
	    }
	} else {
	    opnick = clients[c->blackpos]->nick;
	    nick = clients[c->whitepos]->nick;
	    c->turn = c->blackpos;
	    scor = &c->score_w;
	    switch (c->game) {
	    case PORTES:
		home = 0;
		ophome = 25;
		break;
	    case PLAKWTO:
	    case FEYGA:
		home = 25;
		ophome = 0;
		break;
	    }
	}
	if (c->board[home].bpop == 15 || c->board[home].wpop == 15) {
	    (*scor)++;
	    if (c->board[ophome].wpop == 0 && c->board[ophome].bpop == 0) {

		/*-- double --*/
		(*scor)++;
	    }
	    rotate++;
	    if (*scor >= c->winscore) {
		win++;
	    }
	}
	return report_chan(pos, c, win, rotate, nick, opnick);
    }
    if (eq(cmd, "give")) {
	extern chan *chans[];
	chan *c;
	if (!strlen(arg) || ((k = atoi(arg)) > 2) || k < 1) {
	    return response(fd, pos, USG_GIVE);
	}
	if ((clients[pos]->status & PLAYER) != PLAYER) {
	    return response(fd, pos, MSG_NO_PLAYER);
	}
	c = chans[clients[pos]->chan];
	c->give = k;
	c->givepos = pos;
	j = pos == c->blackpos ? c->whitepos : c->blackpos;
	sprintf(buf, MSG_GIVE, clients[pos]->nick, k, clients[j]->nick);
	for (i = 0; i < MAX_CLIENTS; i++) {
	    if (clients[i]->chan == clients[pos]->chan) {
		response(clients[i]->fd, i, buf);
	    }
	}
	if ((clients[j]->status & GUI) == GUI) {
	    sprintf(buf, GUI_GIVE, k);
	    response(clients[j]->fd, j, buf);
	}
	return (0);
    }
    if (eq(cmd, "take")) {
	extern chan *chans[];
	chan *c;
	short *scor;
	int win = 0;
	if ((clients[pos]->status & PLAYER) != PLAYER) {
	    return response(fd, pos, MSG_NO_PLAYER);
	}
	c = chans[clients[pos]->chan];
	if (c->givepos < 0 || c->give > 2 || !c->give) {
	    return response(fd, pos, MSG_NO_GIVE);
	}
	if (c->givepos == pos) {
	    return response(fd, pos, MSG_SELF_GIVE);
	}
	scor = c->blackpos == pos ? &c->score_bl : &c->score_w;
	*scor += c->give;
	c->give = 0;
	if (*scor >= c->winscore) {
	    win = 1;
	}
	return report_chan(pos, c, win, 1, clients[pos]->nick, NULL);
    }
    if (eq(cmd, "resign")) {
	chan *c;
	int looser;
	extern chan *chans[];
	if ((clients[pos]->status & PLAYER) != PLAYER) {
	    return response(fd, pos, MSG_NO_PLAYER);
	}
	c = chans[clients[pos]->chan];
	j = pos == c->blackpos ? c->whitepos : c->blackpos;
	looser = pos == c->blackpos ? c->blackpos : c->whitepos;
	sprintf(buf, MSG_RESIGN, clients[pos]->nick, clients[j]->nick);
	sprintf(outbuf, "%s\r\n%s", buf, GUI_NOCHAN);

	/*--TODO: Victory logging goes here later --*/
	send_to_game(pos, outbuf, buf);
#ifdef DB_LOGGING
	if ((time(NULL) - c->starttime) < MIN_PLAY_TIME) {
	    c->status |= UNOFFICIAL;
	}
	if ((clients[j]->status & REGISTERED) == REGISTERED && (clients[pos]->status & REGISTERED) == REGISTERED && (c->status & UNOFFICIAL) != UNOFFICIAL) {

	    user_log(clients[j]->nick, clients[looser]->nick, c->winnerpoints);
	}
#endif

	destroy_chan(clients[pos]->chan);
	return (0);
    }
    if (eq(cmd, "gui")) {
	clients[pos]->status ^= GUI;
	sprintf(buf, "\r\nG NICK %s", clients[pos]->nick);
	return response(fd, pos, buf);
    }
    if (eq(cmd, "faq")) {
	return response(fd, pos, MSG_FAQ);
    }

/**-- admin commands --*/
    if (!strcmp(clients[pos]->nick, ADMIN_NICK)) {
	if (eq(cmd, "dudu")) {
	    sprintf(buf, "* ADMIN MSG: %s %s", arg, rest);
	    if (!strlen(buf)) {
		return (0);
	    }
	    for (i = 0; i < MAX_CLIENTS; i++) {
		if (clients[i]->fd >= 0) {
		    response(clients[i]->fd, i, buf);
		}
	    }
	    return (0);
	}
	if (eq(cmd, "kill")) {
	    ;
	}
    }

/*-- DICE DEBUG ---*/
    if (eq(cmd, "te")) {
	extern chan *chans[];
	if (clients[pos]->chan < 0) {
	    return response(fd, pos, MSG_NO_CMD);
	}
	bzero(chans[clients[pos]->chan]->dice, 5);
	chans[clients[pos]->chan]->dice[0] = (char) atoi(arg);
	chans[clients[pos]->chan]->dice[1] = (char) atoi(rest);
	return cmd_roll_deb(pos);
    }
    if (eq(cmd, "ls") || eq(cmd, "dir") || eq(cmd, "cd") || eq(cmd, "cat") || eq(cmd, "more")) {
	return response(fd, pos, "*** be serious");
    }
    return response(fd, pos, MSG_NO_CMD);
}
