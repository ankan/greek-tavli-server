#include "config.h"

typedef struct {
    int fd;
    int rand_id;
    char inbuf[BUFSIZ];
    short ignored[MAX_IGNORED];
    short ignored_id[MAX_IGNORED];
    char cmdbuf[BUFSIZ];
    char nick[BUFSIZ];
    char regnick[BUFSIZ];
    char name[BUFSIZ];
    char addr[MY_ADDR_LEN];
    time_t contime;
    int status;
    short chan;
    short inv_pos;
    short inv_pts;
    short startgame;
    unsigned char limit;
} client;


#define BLACK 1
#define WHITE 2
#define PAREA 4	/*-- On a channel but not player. Waching the game --*/
#define FREE_TO_PLAY  8	 /*-- Is available for invitation on a game--*/
#define GUI   16 /*-- He has a GUI client, don't display him ASCII art etc. */
#define SILENCE 32 /*-- Only invitations, board & serious system msges --*/
#define PLAYER  64
#define REGISTERED 128
#define ISOP  256 /*-- Is an operator --*/
#define HARD_OP  512 /*-- Is a 1st class operator, passwd file defined --*/
#define MUTED  1024 /*-- Has been muted by an operator --*/

/*--- Bord has 24+2 columns --*/
typedef struct {
    short bpop;	 /*--Black poulia population in the column --*/
    short wpop;	 /*--White    ''  	''	''	'' --*/
    short top;	    /*-- Useful only in plakwto. BLACK/WHITE --*/
} col;

#define BOARD_SIZE 2048
typedef struct {
    short ban[MAX_BANNED];
    short blackpos;		/*position of the black player in *clients[] */
    short whitepos;		/*     ''         white   ''        ''       */
    short score_bl;
    short score_w;
    short status;
    short give;
    short givepos;
    short winscore;
    short winnerpoints;
    char asciib[BOARD_SIZE];
    char asciibout[BOARD_SIZE];
    short turn;	 /*-- whitepos/blackpos*/
    time_t tlimit;
    time_t starttime;
    char move[40];
    char dice[5];
    short mtoex; /*--How many moves to expect from a player (after check)-*/
    short game;	     /*--portes,plakwto,feyga--*/
    col board[26];
} chan;

/* chan.status holds:
 * PRIVE
 * BLACK/WHITE (Whos turn is to play)
 */
#define PRIVE 4
#define ROLLED 8
#define UNOFFICIAL 16
#define QUIET_BLACK 32
#define QUIET_WHITE 64

enum { PORTES, PLAKWTO, FEYGA };
