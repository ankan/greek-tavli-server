
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pwd.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <locale.h>
#include <time.h>
#include "config.h"
#include "tavtypes.h"
#include "prototypes.h"
#include "messages.h"
unsigned short clicnt = 0, chancnt = 0;
fd_set totalset, readset;
int ena = 1;
int listenfd, confd, nullfd, maxfd;
client *clients[MAX_CLIENTS];
chan *chans[MAX_CHANS];
client nullcl;
chan nullchan;
struct sigaction timer, ignore;
char outbuf[BUFSIZ];
int main()
{
    struct sockaddr_in server, remote;
    int remotelen, i;
    struct passwd *p;
    if (getuid() != 0) {
	fprintf(stderr, "Only root can start tavli server\n");
	exit(0);
    }
    bzero(&nullcl, sizeof(nullcl));
    nullcl.fd = -1;
    nullcl.chan = -1;
    for (i = 0; i < MAX_CLIENTS; i++) {
	clients[i] = &nullcl;
    }
    bzero(&nullchan, sizeof(nullchan));
    nullchan.game = -1;
    for (i = 0; i < MAX_CHANS; i++) {
	chans[i] = &nullchan;
    }

    /*------ listening socket initialization ---------*/
    bzero(&server, sizeof(server));
    server.sin_family = MY_AF;
    server.sin_addr.s_addr = htonl(inet_addr(BIND_ADDR));
    server.sin_port = htons(MY_PORT);
    if ((listenfd = socket(MY_AF, SOCK_STREAM, 0)) < 0) {
	perror("socket");
	exit(1);
    }
    if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &ena, sizeof(ena)) < 0) {
	perror("setsockopt");
	exit(1);
    }
    if (setsockopt(listenfd, SOL_SOCKET, SO_KEEPALIVE, &ena, sizeof(ena)) < 0) {
	perror("setsockopt");
	exit(1);
    }
    if (bind(listenfd, (struct sockaddr *) &server, sizeof(server)) != 0) {
	perror("bind");
	exit(1);
    }
    if (listen(listenfd, MY_BACKLOG) < 0) {
	perror("listen");
	exit(1);
    }
    /*----- let's become a daemon. Booooou! -------*/
    if (fork() != 0) {
	exit(0);		/*-- bye bye grandpa! --*/
    }
    setsid();
    sigaction(SIGHUP, &ignore, NULL);
    if (fork() != 0) {
	exit(0);		/*-- bye bye dad! --*/
    }
    umask(0);
    chdir("/");
    nullfd = open("/dev/null", O_RDWR);

    /*------- Drop root privilegdes. ----------*/
    p = getpwnam(MY_USER);
    if (setgid(p->pw_gid) < 0) {
	perror("setgid");
	exit(1);
    }
    if (setuid(p->pw_uid) < 0) {
	perror("setuid");
	exit(1);
    }
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    /*---- It's a good practice to dup std[in,out,err] but we are fd hungry
     *---- Uncomment it if you like
        dup2(nullfd, STDIN_FILENO);
        dup2(nullfd, STDOUT_FILENO);
        dup2(nullfd, STDERR_FILENO);
     -----------------------------------------------------------------------*/
    /*--------- End of daemonization. --------------*/
    setlocale(LC_CTYPE, "el_GR");
    srand(time(NULL)); /*-- Dice generator initialization --*/
    openlog(NULL, 0, MY_LOG);
    maxfd = listenfd;
    FD_SET(listenfd, &totalset);

   /*--- set up timer --*/
    sigemptyset(&timer.sa_mask);
    sigemptyset(&ignore.sa_mask);
    timer.sa_flags |= SA_RESTART;
    timer.sa_handler = kariola_resign;
    ignore.sa_handler = SIG_IGN;
    sigaction(SIGALRM, &timer, NULL);
    alarm(ALRM_INTERVAL);

    for (;;) {
	int i;
	readset = totalset;
      select_again:
	if (select(maxfd + 1, &readset, NULL, NULL, NULL) < 0) {
	    if (errno == EINTR) {
		//      syslog(LOG_ERR, "Efaga SIGALRM!");
		goto select_again;
	    }
	    syslog(LOG_ERR, "Can't select().... Why?");
	    exit(1);
	}
	for (i = 0; i <= maxfd; i++) {
	    if (FD_ISSET(i, &readset)) {
		if (i == listenfd) {

		    /*---- Handle new connection --*/
		    if ((confd = accept(listenfd, (struct sockaddr *) &remote, &remotelen)) < 0) {
			syslog(LOG_ERR, "Can't accept a connection... Why?");
			continue;
		    }
		    if (clicnt >= MAX_CLIENTS) {
			snprintf(outbuf, BUFSIZ, "%s", MSG_TOO_MANY);
			sendall(confd, outbuf);
			close(confd);
			syslog(LOG_ERR, LOGY_TOO_MANY, inet_ntoa(remote.sin_addr));
		    } else {
			wellcome_client(confd, remote);
		    }
		} else {
		    read_client(i);
		}
	    }
	}
    }
    return 0;
}
