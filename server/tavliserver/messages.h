/*-----------------------------
 * Messages and log messages
 *-----------------------------*/

/*-------- LOG MESSAGES -------------*/
#define LOGY_DISCONN "%s (%s) disconnected"
#define LOGY_TOO_MANY "connection attempt from %s but the cafe is full"
#define LOGY_NEW_CONN "connection from %s - %s"
#define LOGY_ERR_PASS "can't open password file: %s"
#define LOGY_INV "%s invites %s (%d pts)"
#define LOGY_ACC "%s accepts invitation from %s (%d pts)"
/*--------- CLIENT MESSAGES ----------*/
#define PROMPT "foufoutos~$ "
#define MSG_TOO_MANY "\r\n*** Too many users...\r\nPlease try later :-(\r\n"
#define MSG_NO_CMD "*** No such command. Try /help"
#define MSG_ARG_BIG "*** Too big argument for command: /%s"
#define MSG_YOU_PLAYER "*** You can't invite someone while you are a player\r\n"
#define MSG_NO_PLAYER  "*** You are not a player"
#define MSG_NO_TURN    "*** It's not your turn"
#define MSG_IS_PLAYER  "*** %s is playing. You can't invite him before finish"
#define MSG_HE_NOT_PLAYER "*** %s is not playing a game"
#define MSG_PRIVE "*** %s is playing a private game. Sorry, you can't watch him"
#define MSG_GO_PRIVE "\r\n*** %s sets the game in private mode\
		      \r\n*** Watchers can't watch any more. Sorry."
#define MSG_STOP_PRIVE "\r\n*** %s sets the game in public mode again"
#define MSG_QUIET "*** This is a quiet game. Nobody can speak."
#define MSG_MUTED "*** You have been muted. You are not allowed to speak."
#define MSG_INVISIBLE "*** You can't speak while you are invisible"
#define MSG_UNREGISTERED "*** You must login to be able to speak."
#define MSG_ALLREADY_QUIET "*** This game is allready set quiet"
#define MSG_SET_QUIET "\r\n*** %s set game in quiet mode. Nobody can speak..."
#define MSG_UNSET_QUIET "\r\n*** %s set game in non quiet mode. Feel free to speak"
#define MSG_KICK       "\r\n*** %s kicked %s"
#define MSG_KICK_REASON "\r\n*** %s kicked %s : %s"
#define MSG_KICKED      "\r\n*** You have been kicked by %s"
#define MSG_KICKED_REASON "\r\n*** You have been kicked by %s : %s"
#define MSG_KICK_YOURSELF "*** No, you can't kick yourself :-)"
#define MSG_KICK_OPP "*** No, you can't kick your opponent :-)"
#define MSG_BAN "\r\n*** %s has been banned by %s"
#define MSG_MANY_BAN "*** Too many banned people on this game...\
	\r\n*** If you don't want an audience use /prive"
#define MSG_ALLREADY_BAN "*** %s is allready banned"
#define MSG_BAN_OPPO "*** No, you can't ban your opponent :-)"
#define MSG_BAN_YOURSELF "*** No, you can't ban yourself :-)"
#define MSG_YOU_BANNED  "*** You can't watch this game. You are banned :-("
#define MSG_IGNORED	"*** You are ignored. %s does not accept msg from you"
#define MSG_ALLREADY_IGNORED "*** %s is allready in your ignore list"
#define MSG_MANY_IGNORED "*** Too many ignored players"
#define MSG_IGNORE_OK	 "*** You will not receive msgs from %s any more"
#define MSG_IGNORE_YOURSELF "*** Is there any reason to ignore yourself ? :-)"
#define MSG_PLAYER_QUIT "*** You are playing a game. You can't quit.\
			 \r\n*** You must first /resign\
			 \r\n*** (and give the victory to your opponent)"
#define MSG_QUITED "\r\n*=* %s quited and left our pretty cafe (server)"
#define MSG_ALLREADY_WATCH "*** You are allready on a game channel.\
			    \r\n*** type /part to leave the game first."
#define MSG_NEW_WATCH "\r\n*+* %s is watching the game"
#define MSG_PLAYER_CANT_PART "*** You are a player. You can't /part\
			      \r\n*** use /resign intstead (Your opponent wins)"
#define MSG_LEFT_CHAN "\r\n*-* %s is no more watching the game"
#define MSG_INV_YOURSELF "*** You can't invite yourself :-)"
#define MSG_INVITE "\r\n***-> %s invites you for a %d points game.\
		    \r\n***-> type: \"/accept %s\" to accept."
#define MSG_INVITE_1_POINT "\r\n***-> %s invites you for a %s 1 points game.\
		    \r\n***-> type: \"/accept %s\" to accept."
#define MSG_CANCEL_INV "\r\n*** Invitation canceled"
#define MSG_NOT_FREE "*** %s does not accept invitations currently"
#define MSG_NO_INV  "*** %s did not invite you."
#define MSG_NO_CHAN  "*** You are not on a channel.\
	\r\n*** You can join a channel using /watch <player>, /invite <user>\
	\r\n*** or /accept <user_who_invited_you>. Try /help"
#define MSG_NO_NICK "*** <%s>: no such user"
#define MSG_NO_OP "*** You are not an operator"
#define MSG_NO_HARDOP "*** Only blue-blooded oerators can do that :-)"
#define MSG_NO_NICK_ON_CHAN "*** <%s>: no such user on your game channel"
#define MSG_MSG "\r\n** <%s> -> %s"
#define MSG_SILENCE "*** <%s> can't hear you. (Is in silent mode)"
#define MSG_SILENCE_ON "*** You are not receiving messages any more\
		    \r\n*** except invitations & various system messages\
		    \r\n*** Type /silence again to exit silent mode"
#define MSG_SILENCE_OFF "\r\n*** You are no more in silent mode"
#define MSG_WHOIS "*** %s is %s from IP address %s\
		   \r\n*** connected since %s"
#define MSG_WHOIS_NONAME "*** %s from IP address %s remains anonymous\
		  \r\n*** connected since %s"
#define MSG_SET_FREE "*** You are available for gaming again"
#define MSG_SET_NOFREE "*** You are declared as not available for games\
	\r\n*** You are not accepting invitations\
	\r\n*** Type /free again to declare you available"
#define MSG_NOB_WAITS "*** Nobody wants to start a game. Sorry..."
#define MSG_NO_GAMES  "*** There are not any games currently"
#define MSG_COUNT "*** Currently %d users connected"
#define MSG_USER "*** %s accepted as your registered nick.\
	\r\n*** Type: /pass <your_password> to identify yourself."
#define MSG_PASS_NO_NICK "*** use the /user command first."
#define MSG_SERR_PASS "*** Something goes wrong here on the server\
	\r\n*** I'm afraid you can't log in this time. Sorry :-(\
	\r\n*** Try later and use your current nick till then..."
#define MSG_BAD_PASS "*** Bad password for nick: %s"
#define MSG_PASS_OK "*** Password accepted. Your nick is now: %s"
#define MSG_DOUBL_LOG_IN "*** Multiple logins not allowed."
#define MSG_SAME_IP_LOGIN "*** Please only one login per IP address"
#define MSG_PLAYER_LOG_IN "*** You can't change your nick during the game."
#define MSG_START_ROLL " %s (black) rolls %d, %s (white) rolls %d.\
 %s plays first."
#define MSG_FIRST_ROLL "*** You have not rolled. (/roll)"
#define MSG_ALLREADY_ROLLED "*** You have allready rolled"
#define MSG_COUNT_MOVE "*** We expect %d moves, not %d."
#define MSG_INV_MOVE "*** Invalid move: %s"
#define MSG_INV_SER_MOVE "*** Move no %d is invalid"
#define MSG_INV_START "*** Invalid start position: %s"
#define MSG_INV_DICE  "*** Invalid dice: %s"
#define MSG_INV_TAKOS "*** Takos has a meaning in PORTES only"
#define MSG_GIVE "\r\n*** %s wants to give up this set giving %d points\
		\r\n*** %s type /take to accept"
#define MSG_NO_GIVE "*** I'm afraid that nobody gave you the game..."
#define MSG_SELF_GIVE "*** No, you can't give the game with one hand and take it with the other :-)"
#define MSG_RESIGN "\r\n*** %s resigns, %s wins the game (Huraaaaah!!!)"
#define MSG_DISCON "\r\n*** %s disconnected, %s wins the game (Huraaaah!!!)"
#define MSG_WIN "\r\n*** %s wins the game. Score: %d-%d. (Huraaaaah!!!)"
#define MSG_TIME_EXCEEDED "\r\n*** Time limit exceeded for %s. %s wins the game"
#define MSG_BANNER "\r\n===========================================\
                    \r\n  Wellcome to the first Greek Tavli Server\
		    \r\n  Design & Programming: Antonis Kanavouras\
		    \r\n\
		    \r\n  Your nick is currently %s\
		    \r\n  If you have a registered nick use the\
                    \r\n  /user & /pass commands\
		    \r\n\
		    \r\n  If not you can pick one from:\
		    \r\n         www.exares.gr\
		    \r\n\
		    \r\n  If you can't see what you are typing\
		    \r\n  set local echo on manualy\
		    \r\n\
		    \r\n  Commands begin with a '/' like in IRC\
		    \r\n  Try /help for help, /faq for FAQ\
		    \r\n\
		    \r\n  This server is still experimental\
		    \r\n  Please report any bugs to %s\
		    \r\n===========================================\r\n\r\n%s"
#define MSG_FAQ \
"*** 1. Why do you draw the whole board on every roll? Isn't it a waste of time\r\n\
*** and bandwidth?\r\n\
*** A: It is. But I desided to draw a large comfortable board.\r\n\
*** This means also that if you receive an one line message \"x rolls 2-3\" \r\n\
*** plus prompt you have to ask for /board anyway in a 24x80 terminal because\r\n\
*** the first board line goes away. Now the board comes automaticaly.\r\n\
***\r\n\
*** 2. When will there be an option to start the game e.g. with FEYGA or not to\r\n\
*** play PLAKWTO at all  etc etc...?\r\n\
*** A: Never! Here we play Greek Tavli. If you want to play only FEYGA then\r\n\
*** /give PLAKWTO and PORTES.\r\n\
*** There is a way for everything.\r\n\
***\r\n\
*** 3. I would prefer an other /move notation, e.g. /m 12 13 15 18\r\n\
*** A: This is the chosen notation (based on the BSD Backgammon notation).\r\n\
*** Love it or leave it. Please don't ask me to recode the command handling.\r\n\
*** Be patient and learn it. Sorry.\r\n\
***\r\n\
*** 4. Where can I send my F.A. or non F.A Questions?\r\n\
*** A: To info@exares.gr"
/*---- COMMAND USAGE MESSAGES ---------*/
#define USG_INVITE "*** usage: /invite <user>"
#define USG_ACCEPT "*** usage: /accept <user>"
#define USG_WATCH  "*** usage: /watch  <user>"
#define USG_KICK   "*** usage: /kick <user>"
#define USG_MUTE   "*** usage: /mute <user>"
#define USG_OP   "*** usage: /op <user>"
#define USG_WHOIS  "*** usage: /whois <user>"
#define USG_NAME   "*** usage: /name <your real name>"
#define USG_BAN    "*** usage: /ban <user>"
#define USG_IGNORE "*** usage: /ignore <user>"
#define USG_USER   "*** usage: /user <your_registered_nick>"
#define USG_MOVE "*** usage: /move <position1>/<dice1>,<position2>/<dice2>..."
#define USG_GIVE "*** usage: /give <points> (1 or 2)"
/*--- Special messages leading tokens ---*/

#define TOKEN_NAMES "### Watching this game: "
#define TOKEN_GAMES "@ "
#define TOKEN_FREE "% "

/*--- GUI PROTOCOL ------*/
#define GUI_NICK "\r\nG NICK %s"
#define GUI_NOCHAN "G NOCHAN"
#define GUI_CHAN_PLAYER "G CHAN P %d %s %d %d %d"
/*-- args: blacknick - blackscore -whitenick -whitescore -die1 -die2 - turn - game" */
#define GUI_CHAN_W "G CHAN W %s %d %s %d %d %d %d %d"
#define GUI_INIT "G INIT %d %d %d %d"
#define GUI_POSITIONS "G POS "
/*-- args: color_who_rolls - die1 - die 2 - moves we expect --*/
#define GUI_ROLL "G ROLL %d %d %d %d"
#define GUI_GIVE "G GIVE %d"
#define GUI_REJECT "\r\nG REJ"
#define GUI_WIN "\r\nG WIN"
/* --args: color_who_moves - move */
#define GUI_MOVE_TURN "G MOVE %d %s"
#define GUI_SEND_GAMES "G GM"
#define  GUI_SEND_WHO  "G WHO"
