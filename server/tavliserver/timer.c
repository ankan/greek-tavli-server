#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include "prototypes.h"
#include "tavtypes.h"
#include "config.h"
#include "messages.h"

int send_to_chan(int pos, int urgent, char *s);
int report_chan(int pos, chan * c, short win, short rotate, char *nick, char *opnick);

void kariola_resign(int signo)
{

    extern chan *chans[];
    extern chan nullchan;
    extern client *clients[];
    short i;
    chan *c;
    time_t now;
    unsigned char diff;
    now = time(NULL);

    for (i = 0; i < MAX_CHANS; i++) {
	if (chans[i] == &nullchan) {
	    continue;
	}
	c = chans[i];
	diff = (unsigned char) (now - c->tlimit);
	if (diff > clients[c->turn]->limit) {
	    short looser, winner;
	    char tbuf[BUFSIZ];
	    looser = c->turn;
	    winner = looser == c->blackpos ? c->whitepos : c->blackpos;
	    /*--- debug ---
	sprintf(tbuf,"client1 limit = %d client2 limit =%d diff =%d\r\n", clients[looser]->limit, clients[winner]->limit, diff);
	send_to_chan(looser,1,tbuf);
	    END DEBUG */
	    sprintf(tbuf, MSG_TIME_EXCEEDED, clients[looser]->nick, clients[winner]->nick);
	    send_to_chan(looser, 1, tbuf);
	    report_chan(winner, chans[i], 1, 0, clients[winner]->nick, clients[looser]->nick);

	} else if (diff >= CRITICAL_TIME) {
		    /*--- debug ---
	char tbuf[BUFSIZ] ;
	sprintf(tbuf,"diff =%d\r\n", diff);
	send_to_chan(c->turn,1,tbuf);
	    END DEBUG */

	    clients[c->turn]->limit = HARD_TIME_LIMIT;
	}
    }

    alarm(ALRM_INTERVAL);
    return;
}
