

#include "help.h"
#include "prototypes.h"
#include <string.h>

#define eq(x,y) !strcmp(x,y)
int do_help(int fd, int pos, char *s)
{
    if (!strlen(s) || eq(s, "help")) {
	return response(fd, pos, H_HELP);
    }
    if (eq(s, "move")) {
	return response(fd, pos, H_MOVE);
    }
    if (eq(s, "accept")) {
	return response(fd, pos, H_ACCEPT);
    }
    if (eq(s, "invite")) {
	return response(fd, pos, H_INVITE);
    }
    if (eq(s, "ignore")) {
	return response(fd, pos, H_IGNORE);
    }
    if (eq(s, "names")) {
	return response(fd, pos, H_NAMES);
    }
    if (eq(s, "name")) {
	return response(fd, pos, H_NAME);
    }
    if (eq(s, "prive")) {
	return response(fd, pos, H_PRIVE);
    }
    if (eq(s, "quiet")) {
	return response(fd, pos, H_QUIET);
    }
    if (eq(s, "games")) {
	return response(fd, pos, H_GAMES);
    }
    if (eq(s, "who")) {
	return response(fd, pos, H_WAIT);
    }
    if (eq(s, "free")) {
	return response(fd, pos, H_FREE);
    }
    if (eq(s, "whois")) {
	return response(fd, pos, H_WHOIS);
    }
    if (eq(s, "count")) {
	return response(fd, pos, H_COUNT);
    }
    if (eq(s, "kick")) {
	return response(fd, pos, H_KICK);
    }
    if (eq(s, "ban")) {
	return response(fd, pos, H_BAN);
    }
    if (eq(s, "silence")) {
	return response(fd, pos, H_SILENSE);
    }
    if (eq(s, "user")) {
	return response(fd, pos, H_USER);
    }
    if (eq(s, "pass")) {
	return response(fd, pos, H_PASS);
    }
    if (eq(s, "roll")) {
	return response(fd, pos, H_ROLL);
    }
    if (eq(s, "watch")) {
	return response(fd, pos, H_WATCH);
    }
    if (eq(s, "give")) {
	return response(fd, pos, H_GIVE);
    }
    if (eq(s, "take")) {
	return response(fd, pos, H_TAKE);
    }
    if (eq(s, "resign")) {
	return response(fd, pos, H_RESIGN);
    }
    if (eq(s, "board")) {
	return response(fd, pos, H_BOARD);
    }
    if (eq(s, "msg")) {
	return response(fd, pos, H_MSG);
    }
    if (eq(s, "quit")) {
	return response(fd, pos, H_QUIT);
    }
    if (eq(s, "part")) {
	return response(fd, pos, H_PART);
    }
    if (eq(s, "faq")) {
	return response(fd, pos, H_FAQ);
    }
    return response(fd, pos, MSG_NO_CMD);
}
