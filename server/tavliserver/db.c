#include <stdio.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"

#ifdef DB_LOGGING

void user_log(char *nick, char *looser_nick, short pts)
{
    pid_t pid;
    char points[4];
    sprintf(points, "%d", pts);

    if ((pid = vfork()) < 0) {
	syslog(LOG_ERR, "vfork sucks on user_log()");
	return;
    }
    if (pid > 0) {
	wait(NULL);
	return;
    }
    if (execl(PERL, "log_tavli_user", USER_LOG_PERL, nick, looser_nick, points, NULL) < 0) {
	syslog(LOG_ERR, strerror(errno));
    }
    exit(1);			/*-- in case of an exec error --*/
}

void status_log(char *step, char *what)
{
    pid_t pid;
    if ((pid = vfork()) < 0) {
	syslog(LOG_ERR, "vfork sucks on status_log()");
	return;
    }
    if (pid > 0) {
	wait(NULL);
	return;
    }
    if (execl(PERL, "log_tavli_status", STATUS_LOG_PERL, step, what, NULL) < 0) {
	syslog(LOG_ERR, strerror(errno));
    }
    exit(1);			/*-- in case of an exec error --*/
}

void login_log(char *nick)
{
    pid_t pid;
    if ((pid = vfork()) < 0) {
	syslog(LOG_ERR, "vfork sucks on status_log()");
	return;
    }
    if (pid > 0) {
	wait(NULL);
	return;
    }
    if (execl(PERL, "log_tavli_login", LOGIN_LOG_PERL, nick, NULL) < 0) {
	syslog(LOG_ERR, strerror(errno));
    }
    exit(0);			/*-- in case of an exec error --*/
}

#endif
