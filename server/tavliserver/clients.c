#include <stdio.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <signal.h>
#include "messages.h"
#include "config.h"
#include "tavtypes.h"

#if MY_AF == AF_INET
#include <netinet/ip.h>
#elif MY_AF == AF_INET6
#include <netinet6/ip6.h>
#endif

#include "prototypes.h"

extern client *clients[];
extern fd_set totalset;
extern fd_set readset;
extern client nullcl;
extern chan nullchan;
extern int maxfd;
extern short clicnt;
extern short chancnt;
extern char outbuf[];
extern chan *chans[];

/*-- Prototype needed only by bye_client() --*/
int send_to_chan(int pos, int urgent, char *s);
int send_to_chan_kill(int chanpos, char *s);

void destroy_chan(int chanpos)
{
    int i;
    if (chanpos < 0) {
	return;
    }
    for (i = 0; i < MAX_CLIENTS; i++) {
	if (clients[i]->chan == chanpos) {
	    if ((clients[i]->status & PLAYER) == PLAYER) {
		clients[i]->status &= ~(PLAYER | BLACK | WHITE);
		clients[i]->status |= FREE_TO_PLAY;
	    }
	    clients[i]->chan = -1;
	}
    }
    free(chans[chanpos]);
    chans[chanpos] = &nullchan;
    chancnt--;
    /*-- Report the GUI clients to send the /games command --*/
    for (i = 0; i < MAX_CLIENTS; i++) {
	if ((clients[i]->status & GUI) == GUI) {
	    response(clients[i]->fd, i, GUI_SEND_GAMES);
	}
    }
#ifdef DB_LOGGING
    status_log(MINUS, LOG_STATUS_CHAN);
#endif
}

void bye_client(int fd)
{
    int i, j;
    if (fd < 0) {
	return;
    }
    for (i = 0; i < MAX_CLIENTS; i++) {
	if (clients[i]->fd == fd) {
	    /*
	     *   syslog(LOG_INFO, LOGY_DISCONN, clients[i]->nick, clients[i]->addr);
	     */
 /*--  A player can not /quit before /resign. But if a player disconnects
  *-- abnormall he leaves some things hanging... So:
  *-- 1. check if clients[i] is a player and if so 2. give the victory to the
  *--opponent, 3. Set the flags to the chann members 4. destroy the channel
  */
	    if ((clients[i]->status & PLAYER) == PLAYER) {
		chan *c;
		char reg = 0;
		short chanpos = clients[i]->chan;
		char nick[42];
		int opp = 0;
		c = chans[clients[i]->chan];
		opp = c->blackpos == i ? c->whitepos : c->blackpos;
		snprintf(nick, 40, "%s", clients[i]->nick);
		if ((clients[i]->status & REGISTERED) == REGISTERED) {
		    reg = 1;
		}
		free(clients[i]);
		clients[i] = &nullcl;
		FD_CLR(fd, &totalset);
		FD_CLR(fd, &readset);
		close(fd);
		clicnt--;
		/*-- Report the GUI clients to send the /who command --*/
		for (j = 0; j < MAX_CLIENTS; j++) {
		    if ((clients[j]->status & GUI) == GUI) {
			response(clients[j]->fd, j, GUI_SEND_WHO);
		    }
		}
#ifdef DB_LOGGING
		status_log(MINUS, LOG_STATUS_CLIENT);
#endif
		bzero(outbuf, BUFSIZ);
		sprintf(outbuf, MSG_DISCON, nick, clients[opp]->nick);

		/*-- Victory logging --*/
#ifdef DB_LOGGING
		if ((time(NULL) - chans[chanpos]->starttime) < MIN_PLAY_TIME) {
		    chans[chanpos]->status |= UNOFFICIAL;
		}
		if ((clients[opp]->status & REGISTERED) == REGISTERED && reg == 1 && (chans[chanpos]->status & UNOFFICIAL) != UNOFFICIAL) {

		    user_log(clients[opp]->nick, nick, chans[chanpos]->winnerpoints);
		}
#endif
		send_to_chan_kill(chanpos, outbuf);
		destroy_chan(chanpos);
		return;
	    }
	    if (clients[i]->chan > -1) {
		short chanpos = clients[i]->chan;
		char nick[40];
		strncpy(nick, clients[i]->nick, 39);
		free(clients[i]);
		clients[i] = &nullcl;
		bzero(outbuf, BUFSIZ);
		sprintf(outbuf, MSG_QUITED, nick);
		for (j = 0; j < MAX_CLIENTS; j++) {
		    if (clients[j]->chan == chanpos) {
			response(clients[j]->fd, j, outbuf);
		    }
		}
	    } else {
		free(clients[i]);
		clients[i] = &nullcl;
	    }
	    FD_CLR(fd, &totalset);
	    FD_CLR(fd, &readset);
	    close(fd);
	    clicnt--;
    /*-- Report the GUI clients to send the /who command --*/
	    for (j = 0; j < MAX_CLIENTS; j++) {
		if ((clients[j]->status & GUI) == GUI) {
		    response(clients[j]->fd, j, GUI_SEND_WHO);
		}
	    }
#ifdef DB_LOGGING
	    status_log(MINUS, LOG_STATUS_CLIENT);
#endif
	    return;
	}
    }

}


void wellcome_client(int fd, struct sockaddr_in remote)
{
    int j, i;
    char WONTECHO[] = { 255, 252, 1, 0 };
    extern struct sigaction ignore, timer;


    for (j = 0; j < MAX_CLIENTS; j++) {
	if (strcmp(clients[j]->addr, inet_ntoa(remote.sin_addr)) == 0) {
	    close(fd);
	    return;
	}
	if (clients[j]->fd == -1) {
	    sigaction(SIGALRM, &ignore, NULL);
	    if ((clients[j] = malloc(sizeof(client))) == NULL) {
		sigaction(SIGALRM, &timer, NULL);
		clients[j] = &nullcl;
		close(fd);
		return;
	    }
	    sigaction(SIGALRM, &timer, NULL);
	    FD_SET(fd, &totalset);
	    maxfd = maxfd > fd ? maxfd : fd;
#if MY_AF == AF_INET
#ifdef IPTOS_LOWDELAY
	    i = IPTOS_LOWDELAY;
	    setsockopt(fd, IPPROTO_IP, IP_TOS, &i, sizeof(i));

#endif				/* IPTOS_LOWDELAY */
#ifdef SO_KEEPALIVE
	    i = 1;
	    setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &i, sizeof(i));

#endif				/* SO_KEEPALIVE */
#endif				/* AF_INET */
	    bzero(clients[j], sizeof(client));
	    clients[j]->contime = time(NULL);
	    clients[j]->fd = fd;
	    clients[j]->rand_id = (short) rand();
	    sprintf(clients[j]->name, "%s", "");
	    sprintf(clients[j]->nick, "magkas%d", j);
	    sprintf(clients[j]->regnick, "%s", "");
	    sprintf(clients[j]->addr, "%s", inet_ntoa(remote.sin_addr));
	    clients[j]->chan = -1;	/* not in a channel */
	    clients[j]->status |= FREE_TO_PLAY;
	    clients[j]->inv_pos = -1;	/* nobody invited */
	    for (i = 0; i < MAX_IGNORED; i++) {
		clients[j]->ignored[i] = -1;
	    }
	    clicnt++;
	    snprintf(outbuf, BUFSIZ, MSG_BANNER, clients[j]->nick, MY_BUG_MAIL, PROMPT);
	    sendall(fd, outbuf);
	    sendall(fd, WONTECHO);
/*
 *   syslog(LOG_INFO, LOGY_NEW_CONN, inet_ntoa(remote.sin_addr), clients[j]->nick);
 */
    /*-- Report the GUI clients to send the /who command --*/
	    for (j = 0; j < MAX_CLIENTS; j++) {
		if ((clients[j]->status & GUI) == GUI) {
		    response(clients[j]->fd, j, GUI_SEND_WHO);
		}
	    }
#ifdef DB_LOGGING
	    status_log(PLUS, LOG_STATUS_CLIENT);
#endif
	    break;
	}
    }

}

int response(int fd, int pos, char *s)
{
    char buf[BUFSIZ];
    if (s == NULL) {
	snprintf(buf, BUFSIZ - (1 + strlen(PROMPT)), "\r\n");
    } else {
	snprintf(buf, BUFSIZ - (1 + strlen(PROMPT)), "%s%s", s, "\r\n");
    }
    if (!(clients[pos]->status & GUI)) {
	strcat(buf, PROMPT);
    }
    return sendall(fd, buf);
}

int read_client(int fd)
{
    int i, n;
    char cmdok = 0;
    char buf[BUFSIZ];
    char *tmp1;
    char WONTECHO[] = { 255, 252, 1, 0 };
    char DOECHO[] = { 255, 253, 1, 0 };
    bzero(buf, BUFSIZ);
    for (i = 0; i < MAX_CLIENTS; i++) {
	if (clients[i]->fd == fd) {
	    break;
	}
    }
    if ((n = recv(fd, buf, BUFSIZ - 1, 0)) < 0) {
	bzero(outbuf, BUFSIZ);
	sprintf(outbuf, " recv: %s", strerror(errno));
	syslog(LOG_ERR, outbuf);
	return (-1);
    }
    if (n == 0 || n > MAX_UINPUT * 2) {
	bye_client(fd);
	return (1);
    }

    strncat(clients[i]->inbuf, buf, ((BUFSIZ / 8) - 2) - strlen(clients[i]->inbuf));

    if (strchr(clients[i]->inbuf, LF) != NULL || strlen(clients[i]->inbuf) >= (BUFSIZ / 8) - 2) {
	tmp1 = strtok(clients[i]->inbuf, "\n");
	cmdok = 1;
	strncpy(clients[i]->cmdbuf, tmp1, (MAX_UINPUT - 2));

	if ((strchr(clients[i]->cmdbuf, ESC)) != NULL) {
	    bzero(clients[i]->cmdbuf, MAX_UINPUT);
	    sprintf(buf, "\r\n*** Escape sequenses (e.g. arrows) not allowed\r\n");
	    response(fd, i, buf);
	    cmdok = 0;
	}
	if (strstr(clients[i]->cmdbuf, DOECHO) != NULL) {
	    sendall(fd, WONTECHO);
	}
	if (cmdok) {
	    proc_cmd(fd, i);
	}

	while ((tmp1 = strtok(NULL, "\n")) != NULL) {
	    cmdok = 1;
	    strncpy(clients[i]->cmdbuf, tmp1, (MAX_UINPUT - 2));
	    if ((strchr(clients[i]->cmdbuf, ESC)) != NULL) {
		bzero(clients[i]->cmdbuf, MAX_UINPUT);
		sprintf(buf, "\r\n*** Escape sequenses (e.g. arrows) not allowed\r\n");
		response(fd, i, buf);
		cmdok = 0;
	    }
	    if (strstr(clients[i]->cmdbuf, DOECHO) != NULL) {
		sendall(fd, WONTECHO);
	    }
	    if (cmdok) {
		proc_cmd(fd, i);
	    }
	}
	bzero(clients[i]->inbuf, BUFSIZ / 8);
    }
    return (0);

}
