/*------------------------------------------------------------
 * Definitions of varius constants for the tavli server
 *-----------------------------------------------------------*/

#define MY_PORT 2222
#define MY_BUG_MAIL "bugs@exares.gr"
#define ADMIN_NICK "ankan"

/* Define DB_LOGGING if you use helpers to do logging in an RDBMS
 * Then edit db.c and the definitions at the last part of this file
 */
#define DB_LOGGING
/* AF_INET6 not yet supported. But we define MY_AF for future use */
#define MY_AF   AF_INET

#ifndef INET_ADDRSTRLEN
#include <arpa/inet.h>
#endif
#if MY_AF == AF_INET
#ifndef INET_ADDRSTRLEN
#include <netinet/in.h>
#endif
#define MY_ADDR_LEN INET_ADDRSTRLEN
#elif MY_AF == AF_INET6
#ifndef INET6_ADDRSTRLEN
#include <netinet6/in6.h>
#endif
#define MY_ADDR_LEN INET6_ADDRSTRLEN
#endif

/*--------------------------------------------------------------------
 * If you want a specific address to bind define it  as following:
 * #define   BIND_ADDR "123.145.133.23" (double quoted) 
 *-------------------------------------------------------------------*/
#define BIND_ADDR "0.0.0.0"	/*INADDR_ANY */

/* --------------------------------------------------------------------
 * We use the synchronous multiplexing I/O model and especially the 
 * select() way. So, when defining the number of clients that can be
 * connected the same time take care that FD_SETSIZE is large enough.
 * I set it to 1020 as in Linux 2.4.3 I have by default an FD_SETSIZE of 1024
 * You  may have to tweak your OS kernel to support as many fd's as you
 * need. The same for fd's per proccess (bash~$ help ulimit)
 *-------------------------------------------------------------------------*/
#define MAX_CLIENTS 256
#define MAX_CHANS MAX_CLIENTS/2

/*-- Maximum lenght of user input line. 256 is more than 3 x enough --*/
#define MAX_UINPUT 256		/* 253 + CR + LF + 0 */

/*--- backlog for listen() ---- */
#define MY_BACKLOG 48

/*---------------------------------------------------------------------
 * syslog facility for normal events and errors. I set it to
 * local1. This means that I added the following lines to my
 * /etc/syslog.conf:

  local1.info		/var/log/tavli_events
  local1.err		/var/log/tavli_error

* do something similar
*-----------------------------------------------------------------------*/
#define MY_LOG  LOG_LOCAL1

/*---- The password file ----*/
#define MY_PASS_FILE "/etc/tavlipass"
/*----------------------------------------------------------------------
 * I use to create a user just to be the proccess owner. Be carefull, that
 * user must *not* belong primary to the root group. 
 * (e.g. user tavli who belongs only to group tavli). 
 * It' s highly recomented to do something similar.
 *--------------------------------------------------------------------*/
#define MY_USER "nobody"

/*--- Maximum banned clients per channel ---*/
#define MAX_BANNED 30
#define MAX_IGNORED 30
#define WINSCORE 7
#define START_GAME PORTES
#define TIMELIMIT 180
#define HARD_TIME_LIMIT 60
#define CRITICAL_TIME 120
#define MIN_PLAY_TIME 200
#define ALRM_INTERVAL 120

/*--------- STANDARTS --- DO NOT EDIT ---------*/
#define CR 13
#define LF 10
#define ESC 27

/*---- HELPER APPLICATIONS -DB LOGGING */
#define PERL "/usr/bin/perl"
#define USER_LOG_PERL "/home/httpd/ExaresGlobal/tools/user.pl"
#define STATUS_LOG_PERL "/home/httpd/ExaresGlobal/tools/status.pl"
#define LOGIN_LOG_PERL  "/home/httpd/ExaresGlobal/tools/login.pl"
#define LOG_STATUS_CLIENT "1"
#define LOG_STATUS_CHAN "0"
#define PLUS "1"
#define MINUS "-1"
