#!/usr/bin/perl

use strict ;
use DBI ;
	
my $dsn ="DBI:Pg:dbname=tavli";
my $user ="nobody";
my $passwd ="";
my $dbh = DBI->connect($dsn,$user,$passwd,{RaiseError =>1,AutoCommit=>0});

my $winner= $ARGV[0];
my $looser = $ARGV[1] ;
my $points = $ARGV[3] ;
my $sql ;
my $sql1 ;
my $sth ;
my ($winid, $winrate, $winexp) ;
my $loosid, $loosrate, $loosexp) ;
#-- First select values & update count of games, victory and exp_points

$sql = "SELECT id, rate, exp_points FROM users WHERE nick ='$nick'" ;
$sth = $dbh->prepare($sql);
$sth->execute();
($winid, $winrate, $winexp) = $sth->fetchrow();
$sql = "SELECT id, rate, exp_points FROM users WHERE nick ='$looser'" ;
$sth = $dbh->prepare($sql);
$sth->execute();
($loosid, $loosrate, $loosexp)  = $sth->fetchrow();
$sth->finish() ;
$sql ="INSERT INTO history (winner, looser) VALUES ($winid,$loosid)";	 
$dbh->do($sql);
$sql = "UPDATE users set vic = (vic + 1) WHERE id=$winid";
$dbh->do($sql);
$sql = "UPDATE users set  epx_points = (exp_points + $points WHERE id=$winid OR id=$loosid";
$dbh->do($sql);

#--- Now calculate the rating steps for winner and looser

my $diff = abs($winrate - $loosrate);
my $fact = 1/(10 ** ($diff * sqrt($points)/2000) + 1);
my $prop = ($winrate > $loosrate) ? $fact : 1 - $fact ;
$dbh->commit();
$dbh->disconnect();

1;
