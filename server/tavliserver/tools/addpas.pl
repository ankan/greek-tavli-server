#!/usr/bin/perl

use strict ;
use Fcntl ':flock';

my $passfile = "/etc/tavlipass";
my $user = $ARGV[0];
my $pass1 = $ARGV[1];
my $pass2 = $ARGV[2];

if ($pass1 != $pass2){
	die "passwords are not equal\n";
}
open(PASS,"<$passfile")|| die "Can't open $passfile\n";
while(<PASS>){
	 /^(.*):.*/ ;
	if($user eq $1){
		die "User exists\n";
	}
}
close(PASS);
my $enc = crypt($pass1,$pass1);

open(PASS,">>$passfile")||die "Can't open $passfile for append\n";
flock(PASS,LOCK_EX);
seek(PASS,0,2);
print PASS "$user:$enc\n";
close(PASS);
print "User added\n";
1;
