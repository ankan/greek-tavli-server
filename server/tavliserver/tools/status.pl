#!/usr/bin/perl

use strict ;
use DBI ;
	
my $dsn ="DBI:Pg:dbname=tavli";
my $user ="nobody";
my $passwd ="";
my $dbh = DBI->connect($dsn,$user,$passwd,{RaiseError =>1,AutoCommit=>0});

my $step = $ARGV[0];
my $what = $ARGV[1];
my $sql ;
if($what){
	 $sql = "UPDATE online set hm=(hm + $step)";
}else {
	 $sql = "UPDATE games set hm=(hm + $step)";
}

$dbh->do($sql);
$dbh->commit();

if(! $what && $step > 0){
	$sql ="update totgames set total = (total + 1)";
	$dbh->do($sql);
	$dbh->commit();

}

$dbh->disconnect();

1;
