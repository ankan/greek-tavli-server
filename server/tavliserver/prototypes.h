#ifndef INADDR_ANY
#include <sys/types.h>
#include <sys/socket.h>
#endif
/*-- Include just to avoid annoying compiler warnings
 *-- because of the sockaddr_in type --*/

int sendall(int s, char *buf);
void bye_client(int fd);
void destroy_chan(int chanpos);
void wellcome_client(int fd, struct sockaddr_in remote);
int read_client(int fd);
int proc_cmd(int fd, int i);
int response(int fd, int pos, char *s);
int do_help(int fd, int pos, char *s);
void user_log(char *nick, char *looser_nick, short pts);
void status_log(char *step, char *what);
void login_log(char *nick);
void kariola_resign(int signo);
