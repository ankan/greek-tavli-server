
#include "tavli.h"
#include <signal.h>

#define eq(x,y) !strcmp(x,y)


int send_to_chan(int pos, int urgent, char *s)
{
    int i;
    extern client *clients[];
    if (clients[pos]->fd < 0) {
	return (1);
    }
    for (i = 0; i < MAX_CLIENTS; i++) {
	if (clients[i]->fd > -1 && clients[i]->chan == clients[pos]->chan) {
	    if (!urgent) {
		if ((clients[i]->status & SILENCE) == SILENCE) {
		    continue;
		}
	    }
	    response(clients[i]->fd, i, s);
	}
    }
    return (0);
}
int send_to_game(int pos, char *gui, char *ascii)
{
    int i;
    extern client *clients[];
    if (clients[pos]->chan < 0 || clients[pos]->fd < 0) {
	return (1);
    }
    for (i = 0; i < MAX_CLIENTS; i++) {
	if (clients[i]->chan == clients[pos]->chan) {
	    if ((clients[i]->status & GUI) == GUI) {
		response(clients[i]->fd, i, gui);
	    } else {
		response(clients[i]->fd, i, ascii);
	    }
	}
    }
    return (0);
}

/*-- Usefull only on player disconnection --*/
int send_to_chan_kill(int chanpos, char *s)
{
    int i;
    extern client *clients[];
    char buf[BUFSIZ];
    sprintf(buf, "%s", s);
    strcat(buf, "\r\n");
    strcat(buf, GUI_NOCHAN);
    for (i = 0; i < MAX_CLIENTS; i++) {
	if (clients[i]->chan == chanpos) {
	    if ((clients[i]->status & GUI) == GUI) {
		response(clients[i]->fd, i, buf);
	    } else {
		response(clients[i]->fd, i, s);
	    }
	}
    }
    return (0);
}
int speak_to_chan(int pos, char *s)
{
    extern char outbuf[];
    extern client *clients[];
    extern chan *chans[];
    if (clients[pos]->chan > -1 && ((chans[clients[pos]->chan]->status & QUIET_BLACK) == QUIET_BLACK || (chans[clients[pos]->chan]->status & QUIET_WHITE) == QUIET_WHITE)) {
	return response(clients[pos]->fd, pos, MSG_QUIET);
    }
    if ((clients[pos]->status & MUTED) == MUTED) {
	return response(clients[pos]->fd, pos, MSG_MUTED);
    }
    if ((clients[pos]->status & REGISTERED) != REGISTERED) {
	return response(clients[pos]->fd, pos, MSG_UNREGISTERED);
    }
    if ((clients[pos]->status & FREE_TO_PLAY) != FREE_TO_PLAY) {
	return response(clients[pos]->fd, pos, MSG_INVISIBLE);
    }
    bzero(outbuf, BUFSIZ);
    sprintf(outbuf, "\r\n<%s> ", clients[pos]->nick);
    strncat(outbuf, s, 100);
    send_to_chan(pos, 0, outbuf);
    syslog(LOG_NOTICE, "From IP: %s -- %s", clients[pos]->addr, outbuf);
    return (0);
}
int msg_user(int pos, char *nick, char *s)
{
    int i, j;
    extern char outbuf[];
    extern client *clients[];


    if ((clients[pos]->status & MUTED) == MUTED) {
	return response(clients[pos]->fd, pos, MSG_MUTED);
    }
    if ((clients[pos]->status & REGISTERED) != REGISTERED) {
	return response(clients[pos]->fd, pos, MSG_UNREGISTERED);
    }

    bzero(outbuf, BUFSIZ);
    for (i = 0; i < MAX_CLIENTS; i++) {
	if (!strcmp(nick, clients[i]->nick)) {
	    if ((clients[i]->status & SILENCE) == SILENCE) {
		sprintf(outbuf, MSG_SILENCE, nick);
		return response(clients[pos]->fd, pos, outbuf);
	    }
	    for (j = 0; j < MAX_IGNORED; j++) {
		if (clients[i]->ignored[j] == pos && clients[i]->ignored_id[j] == clients[pos]->rand_id) {
		    sprintf(outbuf, MSG_IGNORED, nick);
		    return response(clients[pos]->fd, pos, outbuf);
		}
	    }

	    snprintf(outbuf, BUFSIZ, MSG_MSG, clients[pos]->nick, s);
	    response(clients[pos]->fd, pos, NULL);	/*give back the prompt */
	    syslog(LOG_NOTICE, "From IP: %s -- %s", clients[pos]->addr, outbuf);
	    return response(clients[i]->fd, i, outbuf);
	}
    }
    sprintf(outbuf, MSG_NO_NICK, nick);
    return response(clients[pos]->fd, pos, outbuf);
}

int invite(int pos, char *nick, int points, int startgame)
{
    extern char outbuf[];
    extern client *clients[];
    int i, j;


    if ((clients[pos]->status & MUTED) == MUTED) {
	return response(clients[pos]->fd, pos, MSG_MUTED);
    }

    bzero(outbuf, BUFSIZ);
    if ((clients[pos]->status & PLAYER) == PLAYER) {
	return response(clients[pos]->fd, pos, MSG_YOU_PLAYER);
    }
    for (i = 0; i < MAX_CLIENTS; i++) {
	if (eq(clients[i]->nick, nick)) {
	    if (i == pos) {
		return response(clients[pos]->fd, pos, MSG_INV_YOURSELF);
	    }
	    for (j = 0; j < MAX_IGNORED; j++) {
		if (clients[i]->ignored[j] == pos && clients[i]->ignored_id[j] == clients[pos]->rand_id) {
		    sprintf(outbuf, MSG_IGNORED, nick);
		    return response(clients[pos]->fd, pos, outbuf);
		}
	    }

	    if ((clients[i]->status & FREE_TO_PLAY) == 0) {
		sprintf(outbuf, MSG_NOT_FREE, nick);
		return response(clients[pos]->fd, pos, outbuf);
	    }
	    if ((clients[i]->status & PLAYER) == PLAYER) {
		sprintf(outbuf, MSG_IS_PLAYER, nick);
		return response(clients[pos]->fd, pos, outbuf);
	    }
	    clients[pos]->inv_pos = i;
	    clients[pos]->startgame = startgame;
	    if (clients[pos]->inv_pts == 1) {
		char *gametype;
		switch (clients[pos]->startgame) {
		case PORTES:
		    gametype = "portes";
		    break;
		case PLAKWTO:
		    gametype = "plakwto";
		    break;
		case FEYGA:
		    gametype = "feyga";
		    break;
		default:
		    gametype = "portes";
		}
		sprintf(outbuf, MSG_INVITE_1_POINT, clients[pos]->nick, gametype, clients[pos]->nick);
	    } else {
		sprintf(outbuf, MSG_INVITE, clients[pos]->nick, clients[pos]->inv_pts, clients[pos]->nick);
	    }
	    response(clients[pos]->fd, pos, NULL);

					     /*- give the prompt back -*/
/*
 *	    syslog(LOG_INFO, LOGY_INV, clients[pos]->nick, clients[i]->nick, clients[pos]->inv_pts);
 */
	    return response(clients[i]->fd, i, outbuf);
	}
    }
    sprintf(outbuf, MSG_NO_NICK, nick);
    return response(clients[pos]->fd, pos, outbuf);
}
void init_game(chan * c, short game)
{
    bzero(c->board, sizeof(c->board));
    switch (game) {
    case PORTES:
	c->board[1].bpop = 2;
	c->board[1].top = BLACK;
	c->board[6].wpop = 5;
	c->board[6].top = WHITE;
	c->board[8].wpop = 3;
	c->board[8].top = WHITE;
	c->board[12].bpop = 5;
	c->board[12].top = BLACK;
	c->board[13].wpop = 5;
	c->board[13].top = WHITE;
	c->board[17].bpop = 3;
	c->board[17].top = BLACK;
	c->board[19].bpop = 5;
	c->board[19].top = BLACK;
	c->board[24].wpop = 2;
	c->board[24].top = WHITE;
	break;
    case PLAKWTO:
	c->board[1].wpop = 15;
	c->board[1].top = WHITE;
	c->board[24].bpop = 15;
	c->board[24].top = BLACK;
	break;
    case FEYGA:
	c->board[12].wpop = 15;
	c->board[12].top = WHITE;
	c->board[24].bpop = 15;
	c->board[24].top = BLACK;
	break;
    }
}
int inv_accept(int pos, char *nick)
{
    extern char outbuf[];
    extern int chancnt;
    extern client *clients[];
    extern chan *chans[];
    extern chan nullchan;
    extern struct sigaction timer, ignore;
    int i, j;
    bzero(outbuf, BUFSIZ);
    if ((clients[pos]->status & PLAYER) == PLAYER) {
	sprintf(outbuf, MSG_NO_INV, nick);
	return response(clients[pos]->fd, pos, outbuf);
    }
    for (i = 0; i < MAX_CLIENTS; i++) {
	if (eq(clients[i]->nick, nick)) {
	    if (clients[i]->inv_pos != pos) {
		sprintf(outbuf, MSG_NO_INV, nick);
		return response(clients[pos]->fd, pos, outbuf);
	    }

	/*-- I put the channel creation code here because there is no other
         *-- way to create a channel other than inv_accept */
	    for (j = 0; j < MAX_CHANS; j++) {
		if (chans[j]->game == -1) {
		    sigaction(SIGALRM, &ignore, NULL);
		    if ((chans[j] = malloc(sizeof(chan))) == NULL) {
			sigaction(SIGALRM, &timer, NULL);
			chans[j] = &nullchan;
			return (-1);
		    }
		    sigaction(SIGALRM, &timer, NULL);
		    bzero(chans[j], sizeof(chan));
		    chans[j]->whitepos = i;
					   /*-- challenger is white.--*/
		    chans[j]->blackpos = pos;
					     /*-- accepter is black */
		    chans[j]->givepos = -1;
		    chans[j]->game = clients[i]->startgame;
		    memset(chans[j]->ban, -1, sizeof(chans[j]->ban));
		    chans[j]->score_bl = chans[j]->score_w = 0;
		    chans[j]->winscore = clients[i]->inv_pts;
		    chans[j]->starttime = time(NULL);
		    switch (chans[j]->winscore) {
		    case 1:
			chans[j]->status |= UNOFFICIAL;
			break;
		    case 3:
			chans[j]->winnerpoints = 3;
			break;
		    case 5:
			chans[j]->winnerpoints = 5;
			break;
		    case 7:
			chans[j]->winnerpoints = 7;
			break;
		    default:
			chans[j]->winnerpoints = 7;
			break;
		    }
		    chancnt++;
		    break;
		}
	    }
	    clients[pos]->chan = clients[i]->chan = j;
	    clients[pos]->status |= (PLAYER | BLACK);
	    clients[i]->status |= (PLAYER | WHITE);
	    clients[pos]->status &= ~FREE_TO_PLAY;
	    clients[i]->status &= ~FREE_TO_PLAY;
	    clients[i]->inv_pos = clients[pos]->inv_pos = -1;
	    clients[i]->limit = clients[pos]->limit = TIMELIMIT;

	    /*--- game init --*/

	    /*-- roll to see who plays first --*/
	    do {
		roll(chans[j]);
	    } while (chans[j]->dice[0] == chans[j]->dice[1]);
	    if (chans[j]->dice[0] > chans[j]->dice[1]) {
		chans[j]->turn = chans[j]->blackpos;
	    } else {
		chans[j]->turn = chans[j]->whitepos;
	    }
	    init_game(chans[j], chans[j]->game);
	    draw_board(chans[j]);
	    strcpy(chans[j]->asciibout, chans[j]->asciib);
	    sprintf(outbuf, MSG_START_ROLL, clients[chans[j]->blackpos]->nick, chans[j]->dice[0], clients[chans[j]->whitepos]->nick, chans[j]->dice[1], clients[chans[j]->turn]->nick);
	    strcat(chans[j]->asciibout, outbuf);

	/*-- report the two players --*/
	    if ((clients[chans[j]->blackpos]->status & GUI) == GUI) {
		snprintf(outbuf, BUFSIZ - 2, GUI_CHAN_PLAYER, BLACK, clients[chans[j]->whitepos]->nick, chans[j]->dice[0], chans[j]->dice[1], chans[j]->game);
		response(clients[chans[j]->blackpos]->fd, pos, outbuf);
	    } else {
		response(clients[chans[j]->blackpos]->fd, pos, chans[j]->asciibout);
	    }
	    if ((clients[chans[j]->whitepos]->status & GUI) == GUI) {
		snprintf(outbuf, BUFSIZ - 2, GUI_CHAN_PLAYER, WHITE, clients[chans[j]->blackpos]->nick, chans[j]->dice[0], chans[j]->dice[1], chans[j]->game);
		response(clients[chans[j]->whitepos]->fd, i, outbuf);
	    } else {
		response(clients[chans[j]->whitepos]->fd, i, chans[j]->asciibout);
	    }
	    syslog(LOG_INFO, LOGY_ACC, clients[pos]->nick, clients[i]->nick, clients[i]->inv_pts);
#ifdef DB_LOGGING
	    status_log(PLUS, LOG_STATUS_CHAN);
#endif
	    /*-- rerport all GUI clients --*/
	    for (i = 0; i < MAX_CLIENTS; i++) {
		if ((clients[i]->status & GUI) == GUI) {
		    response(clients[i]->fd, i, GUI_SEND_GAMES);
		}
	    }
	    chans[j]->tlimit = time(NULL);
	    return (0);
	}
    }
    sprintf(outbuf, MSG_NO_NICK, nick);
    return response(clients[pos]->fd, pos, outbuf);
}
int cmd_roll(int pos)
{
    extern client *clients[];
    extern chan *chans[];
    extern char outbuf[];
    int i;
    chan *c;
    bzero(outbuf, BUFSIZ);
    c = chans[clients[pos]->chan];
    roll(c);
    c->mtoex = cmtoex(c, 1);
    strcpy(c->asciibout, c->asciib);
    if (!c->mtoex) {

		   /*-- He can't move --*/
	i = pos == c->blackpos ? c->whitepos : c->blackpos;
	sprintf(outbuf, "%s rolls %d-%d and can't move. %s please /roll", clients[pos]->nick, c->dice[0], c->dice[1], clients[i]->nick);
	strcat(c->asciibout, outbuf);
	c->turn = i;
    } else {	 /*-- He can move --*/
	sprintf(outbuf, "%s rolls %d-%d. Expecting: %d valid %s.", clients[pos]->nick, c->dice[0], c->dice[1], c->mtoex, c->mtoex == 1 ? "move" : "moves");
	strcat(c->asciibout, outbuf);
	c->status |= ROLLED;
    }

    /* -- color - die1 -die12 -moves we expect -- */
    sprintf(outbuf, GUI_ROLL, c->turn == c->blackpos ? BLACK : WHITE, c->dice[0], c->dice[1], c->mtoex);
    send_to_game(pos, outbuf, c->asciibout);
    return 0;
}
int cmd_roll_deb(int pos)
{
    extern client *clients[];
    extern chan *chans[];
    extern char outbuf[];
    int i;
    chan *c;
    bzero(outbuf, BUFSIZ);
    c = chans[clients[pos]->chan];
    if (c->dice[0] == c->dice[1]) {
	c->dice[2] = c->dice[3] = c->dice[0];
    }
    c->mtoex = cmtoex(c, 1);
    strcpy(c->asciibout, c->asciib);
    if (!c->mtoex) {

		   /*-- He can't move --*/
	i = pos == c->blackpos ? c->whitepos : c->blackpos;
	sprintf(outbuf, "%s rolls %d-%d and can't move. %s please /roll", clients[pos]->nick, c->dice[0], c->dice[1], clients[i]->nick);
	strcat(c->asciibout, outbuf);
	c->turn = i;
    } else {	 /*-- He can move --*/
	sprintf(outbuf, "%s rolls %d-%d. Expecting: %d valid %s.", clients[pos]->nick, c->dice[0], c->dice[1], c->mtoex, c->mtoex == 1 ? "move" : "moves");
	strcat(c->asciibout, outbuf);
	c->status |= ROLLED;
    }
    for (i = 0; i < MAX_CLIENTS; i++) {
	if (clients[i]->chan == clients[pos]->chan) {
	    if ((clients[i]->status & GUI) != GUI) {
		response(clients[i]->fd, i, c->asciibout);
	    } else {

				/*--TODO: do the GUI protocol job -*/
		;
	    }
	}
    }
    return 0;
}
int report_chan(int pos, chan * c, short win, short rotate, char *nick, char *opnick)
{
    extern client *clients[];
    extern char outbuf[];
    char buf[512];
    bzero(outbuf, BUFSIZ);
    if (win) {
	short oppos = pos == c->blackpos ? c->whitepos : c->blackpos;
	sprintf(buf, MSG_WIN, nick, c->score_w, c->score_bl);
	strcpy(outbuf, buf);
	strcat(outbuf, GUI_WIN);
	send_to_game(c->blackpos, outbuf, buf);
#ifdef DB_LOGGING
	if ((time(NULL) - c->starttime) < MIN_PLAY_TIME) {
	    c->status |= UNOFFICIAL;
	}
	if ((clients[pos]->status & REGISTERED) == REGISTERED && (clients[oppos]->status & REGISTERED) == REGISTERED && (c->status & UNOFFICIAL) != UNOFFICIAL) {
	    user_log(nick, clients[oppos]->nick, c->winnerpoints);
	}
#endif
	destroy_chan(clients[c->blackpos]->chan);
	return (0);
    }
    if (rotate) {
	clients[c->blackpos]->limit = clients[c->whitepos]->limit = TIMELIMIT;

	c->turn = pos;	       /*-- winner plays first --*/
	switch (c->game) {
	case PORTES:
	    init_game(c, PLAKWTO);
	    c->game = PLAKWTO;
	    break;
	case PLAKWTO:
	    init_game(c, FEYGA);
	    c->game = FEYGA;
	    break;
	case FEYGA:
	    init_game(c, PORTES);
	    c->game = PORTES;
	    break;
	}
	sprintf(outbuf, "%s wins last game. %s please /roll", nick, nick);
	sprintf(buf, GUI_INIT, c->game, c->score_bl, c->score_w, pos == c->blackpos ? BLACK : WHITE);
	c->give = 0;
	c->givepos = -1;
    } else {
	sprintf(outbuf, "%s moved: %s. %s please /roll", nick, c->move, opnick);
	sprintf(buf, GUI_MOVE_TURN, pos == c->blackpos ? BLACK : WHITE, c->move);
    }
    c->status &= ~ROLLED;
    draw_board(c);
    bzero(c->asciibout, BOARD_SIZE);
    strcpy(c->asciibout, c->asciib);
    strcat(c->asciibout, outbuf);
    send_to_game(c->blackpos, buf, c->asciibout);
    c->tlimit = time(NULL);
    return (0);
}

#undef eq(x,y)
