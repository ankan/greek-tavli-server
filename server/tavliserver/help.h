#include "messages.h"

#define H_HELP \
"\r\n*** Valid commands are:\r\n\
***\r\n\
***	accept	msg	take\r\n\
***	ban	name 	user\r\n\
***	board	names	who\r\n\
***	count	part	watch\r\n\
***	free	pass	whois\r\n\
***	games	prive	faq\r\n\
***	give	quit\r\n\
***	invite	resign\r\n\
***	ignore	roll\r\n\
***	kick	silence\r\n\
***	move	quiet\r\n\
***\r\n\
***	Try /help (or /h) <command>\r\n"
#define H_HELP_NOT_IMP \
"\r\n*** help system not yet implemented"
#define H_MSG \
"\r\n*** Send a private message to another user\r\n\
***  Usage: /msg <user> <yoyr message>\r\n\
***\r\n\
*** See also: /silence"
#define H_SILENSE \
"\r\n*** Set yourself in silent mode\r\n\
*** aka you receive no user or system messages\r\n\
*** except various system messages & invitations\r\n\
*** typing /silence again you leave silent mode.\r\n\
***\r\n\
*** See also: /free"
#define H_FREE \
"\r\n*** Mark you  unavailable to play games.\r\n\
*** You will not receive invitations.\r\n\
*** Typing /free again toggles this mode.\r\n\
***\r\r\
*** See also: /silence"
#define H_IGNORE \
"\r\n*** Usage: /ignore <user>\r\n\
*** <user> can not annoy you with private\r\n\
*** messages any more.\r\n\
***\r\n\
*** See also: /silence, /msg\r\n"
#define H_WAIT \
"\r\n*** /who tells you who is waiting for a game.\r\n\
*** The names you see accept invitations\r\n\
***\r\n\
*** See also: /free, /count"
#define H_GAMES \
"\r\n*** /games shows a list of current games.\r\n\
*** You can /watch one if it is not private and\r\n\
*** you are not /banned from this game.\r\n\
***\r\n\
*** See also: /watch, /prive, /kick, /ban"
#define H_WATCH \
"\r\n*** Usage: /watch <user>\r\n\
*** It's a way to join a game channel.\r\n\
*** (the other is /accept).\r\n\
*** The user to /watch must be a player, not a watcher\r\n\
*** The game channel must not be in private mode and you nust\r\n\
*** not be /banned from this\r\n\
***\r\n\
*** See also: /games, /prive, /kick, /ban"
#define H_KICK \
"\r\n*** Usage: /kick <user>\r\n\
*** Imagine the games in this server as IRC channels.\r\n\
*** The two players are the channel opetators.\r\n\
*** Only them can /kick users, /ban users set /prive mode\r\n\
*** /kick <user> throws him out the game but not out of server.\r\n\
*** He can join (/watch) the game again until he is banned.\r\n\
*** Of course you can not /kick your opponent\r\n\
***\r\n\
*** See also: /watch, /ban, /prive"
#define H_BAN \
"\r\n*** Usage: ban <user>\r\n\
*** Imagine the games in this server as IRC channels.\r\n\
*** The two players are the channel opetators.\r\n\
*** Only them can /kick users, /ban users & set /prive mode\r\n\
*** If you /ban a user watching your game he get kicked and\r\n\
*** can not join any more.\r\n\
*** ATTENTION: There is no /unban.(games are short, shorter than life...)\r\n\
***\r\n\
*** See also:/kick, /watch, /prive"
#define H_PRIVE \
"\r\n*** Usage: /prive\r\n\
*** Imagine the games in this server as IRC channels.\r\n\
*** The two players are the channel opetators.\r\n\
*** Only them can /kick users, /ban users & set /prive mode\r\n\
*** This command sets the game in private mode, aka nobody\r\n\
*** can watch it.\r\n\
*** Typing /prive again exits this mode\r\n\
***\r\n\
*** See also:/watch, /kick, /ban"
#define H_QUIET \
"\r\n*** Usage: /quiet\r\n\
*** Imagine the games in this server as IRC channels.\r\n\
*** The two players are the channel opetators.\r\n\
*** Only them can /kick users, /ban users & set /prive mode\r\n\
*** This command sets the game in quiet mode, aka nobody\r\n\
*** can speak.\r\n\
*** Typing /quiet again exits this mode\r\n\
***\r\n\
*** See also:/prive, /silence, /kick, /ban"
#define H_NAMES \
"\r\n*** If you are on a game  channel (playing or watching)\r\n\
*** you take a list of the users on the same channel\r\n\
*** (and not all the users on server)\r\n\
***\r\n\
*** See also:/whatch, /whois, /games, /who, /count "
#define H_PART \
"\r\n*** Usage: /part\r\n\
*** stop watching a game.\r\n\
*** You can 't /part if you are a player.\r\n\
*** You have to /resign and, of course, give the victory to the opponent\r\n\
***\r\n\
*** See also: /watch, /resign, /give"
#define H_WHOIS \
"\r\n*** Usage /whois <user>\r\n\
*** Gives a piece of information about the <user>\r\n\
***\r\n\
*** See also: /name, /user, /pass, /names, /count"
#define H_USER \
"\r\n*** Usage /user <your_registered_nick>\r\n\
*** This helps you only if you have a registered nick\r\n\
*** After giving your nick you must identify with: /pass\r\n\
*** You can pick a registered nick from www.exares.gr\r\n\
***\r\n\
*** See also: /pass, /name"
#define H_PASS \
"\r\n*** Usage: /pass <your_passwd>\r\n\
*** Identify yourself if you have a registered nick\r\n\
***\r\n\
*** See also: /user, /name"
#define H_NAME \
"\r\n*** Usage: /name <Your real name or anything you want>\r\n\
*** Whenever someone /whois's you he reads whatever you set\r\n\
*** instead of \"anonymous\"\r\n\
*** \r\n\
*** See also:/user, /pass"
#define H_QUIT \
"\r\n*** Bye bye.\r\n\
*** alias: /exit"
#define H_COUNT \
"\r\n*** Usage: /count\r\n\
*** Shows how many users are connected\r\n\
***\r\n\
*** See also: /who, /games, /names, /whois"
#define H_INVITE \
"\r\n*** Usage: /invite <user> [pts | gametype]\r\n\
*** Invite a user to join you and start a <pts> points game\r\n\
*** Valid points are 3, 5 & 7\r\n\
*** The [pts] argument is optional (default 7)\r\n\
***\r\n\
*** Alternatively, instead of a [pts] argument you can determine\r\n\
*** a game type (portes, plakwto, feyga). In that case it will be\r\n\
*** an \"one  shot\" unofficial game (no ratings change).\r\n\
*** Examples:\r\n\
*** /invite boby 3\r\n\
*** /invite boby (= /invite boby 7)\r\n\
*** /invite boby plakwto\r\n\
*** Invited user receives the invitation if he is not currently\r\n\
*** playing another game or is in non-free mode and if he\r\n\
*** accepts he must response with /accept <your_nick>\r\n\
***\r\n\
*** alias:  /inv\r\n\
*** See also: /accept, /free, /games"
#define H_ACCEPT \
"\r\n*** Usage: /accept <user>\r\n\
*** Accept the invitation you received from <user>.\r\n\
*** After that the game starts.\r\n\
***\r\n\
*** alias:  /acc\r\n\
*** See also:/invite "
#define H_BOARD \
"\r\n*** Usefull command if you are playing (or watching) in telnet mode\r\n\
*** It redraws the board if incoming messages mess up your screen\r\n\
***\r\n\
*** Short alias: /b"
#define H_ROLL \
"\r\n*** It rolls the dice\r\n\
*** Short alias: /r"
#define H_RESIGN \
"\r\n*** If you /resign you leave the whole game, not only the set.\r\n\
*** Victory goes to opponent and the channel is destroyed. (It's not\r\n\
*** IRC, we play tavli on channels :-)\r\n\
***\r\n\
*** See also: /give"
#define H_GIVE \
"\r\n*** Usage: /give <points> (1 or 2)\r\n\
*** You can leave a single set if it's only a waste of time\r\n\
*** and give 1 or 2 points to the opponent. The match goes on.\r\n\
*** Your opponent must accept your offer typing /take\r\n\
***\r\n\
*** See also: /take, /resign"
#define H_MOVE \
"\r\n*** The most important server command.\r\n\
*** The argument notation is:\r\n\
*** /move <position1>/<die1>,<position2>/die1> etc.\r\n\
*** Also: position slash die comma position slash die ...\r\n\
*** e.g. /move 23/4,12/2 (you rolled 4-2)\r\n\
*** Slash can also be a - and comma a dot (because I use to mistype)\r\n\
***\r\n\
*** Conclusion:\r\n\
*** 23/4,12/2   23-4.12/2   23-4,12-2 are synonyms.\r\n\
***\r\n\
*** There is a special position in portes, \"takos\" (bar in backgammon).\r\n\
*** It is pos. 25 for white and pos. 0 for black. The /move notation alias\r\n\
*** is \"takos\" or \"ta\". So. if you have poulia on takos and you can\r\n\
*** bring them back on board you have to type something like:\r\n\
*** /move takos/2,18/3 or /move ta/2,18/3\r\n\
*** \r\n\
*** Aliases: /m, /mv (Unix guys always type mv instead of m :-)"
#define H_TAKE \
"\r\n*** Accept the offer and go on.\r\n\
***\r\n\
*** See also: /give"
#define H_FAQ \
"\r\n***\r\n*** Self explanatory\r\n***"
