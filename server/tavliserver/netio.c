#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include "prototypes.h"

/*-----------------------------------------------
 * I found the sendall() function in the Brian "Beej" Hall
 * Guide to Network Programming. It's a perfect function so
 * why should I reinvent the wheel? Thank you Beej!
 * But I changed it a lot:
 * I dropped the int *len arg. We don't need it, we can obtain it from
 * strlen(buf).
 * I  added a SIGPIPE handling because if we send to a half
 * open descriptor our server will teminate. Btw, on an EPIPE I do
 * some client clean up that is tavlid specific.
 * I wonder if it is the same function...
 *---------------------------------------------------*/
int sendall(int s, char *buf)
{
    int total = 0;
    int len = strlen(buf);
    int bytesleft = len;
    int n = 0;
    if (s < 0) {
	return (0);
    }
    while (total < len) {
#ifdef MSG_NOSIGNAL
	n = send(s, buf + total, bytesleft, MSG_NOSIGNAL);
#else
	signal(SIGPIPE, SIG_IGN);
	n = send(s, buf + total, bytesleft, 0);
#endif
	if (n == -1) {
	    switch (errno) {
	    case EPIPE:
	    case EBADF:
		bye_client(s);
		return (0);
	    case EINTR:
	    default:
		continue;
	    }
	}
	total += n;
	bytesleft -= n;
    }
    len = total;
    return n == -1 ? n : 0;
}
